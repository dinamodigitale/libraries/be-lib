import { Directive, ElementRef, Input, OnInit } from '@angular/core'
import { AuthService } from '@be-lib/services/auth-service.service'

@Directive({
  selector: '[isVisibleTo]',
})
export class IsVisibleToDirective implements OnInit {
  @Input() isVisibleTo: string | Array<string>
  nativeElement

  constructor(el: ElementRef, public authService: AuthService) {
    this.nativeElement = el.nativeElement
  }

  ngOnInit() {
    const currentUser = this.authService.getUserInfo() || { role: '', roles: [] }
    if (!Array.isArray(this.isVisibleTo)) {
      if (this.isVisibleTo) {
        this.isVisibleTo = [this.isVisibleTo.toString() as string]
      } else {
        return
      }
    }

    // if (!this.isVisibleTo || !this.isVisibleTo.includes('admin')) {
    //   this.isVisibleTo.push('admin')
    // }

    if (currentUser.roles && currentUser.roles.length) {
      if (
        !(this.isVisibleTo as Array<string>).find((role) => {
          return currentUser.roles.find((userRole) => userRole === role)
        })
      ) {
        this.nativeElement.remove()
      }
    }else{
      if (
        !(this.isVisibleTo as Array<string>).find((role) => {
          return currentUser.role === role
        })
      ) {
        this.nativeElement.remove()
      }
    }

  }
}
