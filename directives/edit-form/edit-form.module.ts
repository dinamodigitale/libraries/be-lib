import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { EditFormDirective } from './edit-form.directive'

@NgModule({
  declarations: [EditFormDirective],
  imports: [CommonModule],
  exports: [EditFormDirective],
})
export class EditFormModule {}
