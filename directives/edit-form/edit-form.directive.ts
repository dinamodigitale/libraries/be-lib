import {
  Directive,
  Input,
  Output,
  EventEmitter,
  ViewChild,
} from '@angular/core'
import { DinamoResourceComponent } from '@be-lib/components/dinamo-resource/dinamo-resource/dinamo-resource.component'
import { User } from '@be-lib/services/auth-service.service'

@Directive({ selector: '[editForm]' })
export class EditFormDirective {
  @Input() item: {
    [x: string]: any
  }
  @Output() itemChange: EventEmitter<any> = new EventEmitter()
  @Input() isEdit: boolean
  @Input() heading: string
  @Input() subTitle: string

  @Output() save: EventEmitter<any> = new EventEmitter()

  @ViewChild(DinamoResourceComponent) resourceComponent: DinamoResourceComponent

  loading = true

  constructor() {}

  update() {
    if (this.resourceComponent && this.resourceComponent.canSave) {
      this.resourceComponent.update().then((res) => {
        this.save.emit(this.item)
      })
    } else {
      this.save.emit(this.item)
    }
  }
}
