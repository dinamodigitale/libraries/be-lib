import {
  Directive,
  Input,
  HostListener,
  Component,
  Inject,
  Output,
  EventEmitter,
} from '@angular/core'

import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog'

@Directive({
  selector: '[dinamoConfirm]',
})
export class ConfirmDirective {
  @Input() dinamoConfirm: {
    message?: string
    title?: string
    noButton?: string
    yesButton?: string
  }

  @Output() click = new EventEmitter()

  constructor(public dialog: MatDialog) {}

  @HostListener('mousedown', ['$event'])
  public onMousedown(event: Event) {
    console.log('mouse down')
    event.stopPropagation()
    this.showDialog(event)
  }

  @HostListener('keydown', ['$event'])
  public onKeydown(event: Event) {
    console.log('mouse down')
    event.stopPropagation()
    this.showDialog(event)
  }

  private showDialog(event: Event) {
    const dialogRef = this.dialog.open(ConfirmDialog, {
      width: '90vw',
      minWidth: '240px',
      maxWidth: '320px',
      data: Object.assign(
        {
          message: 'Are you sure?',
          title: 'Confirmation requested',
          noButton: 'No',
          yesButton: 'Yes',
        },
        this.dinamoConfirm || {}
      ),
    })

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.click.next(event)
      }
    })
  }
}

@Component({
  selector: 'dinamo-confirm-dialog',
  template: `<h1 mat-dialog-title>{{ data.title }}</h1>
    <div mat-dialog-content>
      <p class="mat-h4">{{ data.message }}</p>
    </div>
    <div mat-dialog-actions fxLayout="row" fxLayoutAlign="center center">
      <button mat-button (click)="onNoClick()">{{ data.noButton }}</button>
      <button mat-button color="warn" [mat-dialog-close]="true" cdkFocusInitial>
        {{ data.yesButton }}
      </button>
    </div>`,
})
export class ConfirmDialog {
  constructor(
    public dialogRef: MatDialogRef<ConfirmDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  onNoClick(): void {
    this.dialogRef.close(false)
  }
}
