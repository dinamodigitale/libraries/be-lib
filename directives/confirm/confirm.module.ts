import { NgModule } from '@angular/core'
import { ConfirmDirective, ConfirmDialog } from './confirm.directive'
import { MatDialogModule } from '@angular/material/dialog'
import { MatButtonModule } from '@angular/material/button'

@NgModule({
  imports: [MatDialogModule, MatButtonModule],
  declarations: [ConfirmDirective, ConfirmDialog],
  exports: [ConfirmDirective, ConfirmDialog],
  entryComponents: [ConfirmDialog],
})
export class ConfirmModule {}
