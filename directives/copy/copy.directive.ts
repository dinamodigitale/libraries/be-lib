import { Directive, Input, HostListener } from '@angular/core'
import { SnackService } from '@be-lib/services/snack.service'

@Directive({
  selector: '[dbeCopy]',
})
export class CopyDirective {
  // Parse attribute value into a 'text' variable
  @Input() text: string

  constructor(private snackService: SnackService) {}

  @HostListener('click') copyText() {
    var textArea = document.createElement('textarea')

    textArea.style.position = 'fixed'
    textArea.style.top = '-999px'
    textArea.style.left = '-999px'
    textArea.style.width = '2em'
    textArea.style.height = '2em'
    textArea.style.padding = '0'
    textArea.style.border = 'none'
    textArea.style.outline = 'none'
    textArea.style.boxShadow = 'none'
    textArea.style.background = 'transparent'

    textArea.value = this.text
    document.body.appendChild(textArea)

    textArea.select()

    try {
      const command = document.execCommand('copy')
      if (command) {
        this.snackService.info('Copiato!')
      } else {
        this.snackService.error('Si è verificato un errore durante la copia...')
        console.log('Error')
      }
    } catch (err) {
      this.snackService.error('Si è verificato un errore durante la copia...')
      console.log(err)
    }

    document.body.removeChild(textArea)
  }
}
