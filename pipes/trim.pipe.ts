import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'trim',
})
export class TrimPipe implements PipeTransform {
  transform(value: string, limit: number = 150): string {
    if (value.length <= limit) {
      return value
    } else {
      return value.substr(0, limit - 3) + '...'
    }
  }
}
