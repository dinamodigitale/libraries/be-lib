import { NgModule } from '@angular/core';
import { TrimPipe } from './trim.pipe'

@NgModule({
  imports: [],
  declarations: [TrimPipe],
  exports: [TrimPipe],
})
export class PipesModule {}
