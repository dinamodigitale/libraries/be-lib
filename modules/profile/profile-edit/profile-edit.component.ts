import { Component, OnInit } from '@angular/core'
import { UsersService } from '@app/app/ui/users/users.service'
import { EditFormDirective } from '@be-lib/directives/edit-form/edit-form.directive'
import RestComponent from '@be-lib/services/api-service/rest-component.class'
import { User, AuthService } from '@be-lib/services/auth-service.service'
import { SnackService } from '@be-lib/services/snack.service'
import { StateService } from '@uirouter/core'

@Component({
  selector: 'dbe-profile-edit',
  templateUrl: './profile-edit.component.html',
})
export class ProfileEditComponent extends RestComponent<any> implements OnInit {
  item: User

  hide = true
  heading=""
  constructor(
    private authService: AuthService,
    public usersService: UsersService,
    protected snackService: SnackService,
    protected stateService: StateService
  ) {
    super(usersService, snackService, 'edit', stateService)
    this.item = this.authService.getUserInfo()
    this.heading = this.item.name+' '+this.item.surname
  }

  ngOnInit(): void {
    this.loading = false
  }

  private makePsw(length = 5) {
    let text = ''
    const possible =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    for (let i = 0; i < length; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length))
    }
    return text
  }

  generatePassword() {
    const psw = this.makePsw(8)
    this.item.passwordConfirmation = psw
    this.item.password = psw
    this.hide = false
  }

  get disabledButton() {
    return (
      !this.item.password ||
      !this.item.passwordConfirmation ||
      this.item.password !== this.item.passwordConfirmation ||
      !this.item.email ||
      !this.item.name ||
      !this.item.surname
    )
  }

  updateUser() {
    this.update(this.item._id, this.item)
  }
}
