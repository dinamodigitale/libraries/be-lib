import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'dbe-profile',
  template: '<ui-view></ui-view>',
})
export class ProfileComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
