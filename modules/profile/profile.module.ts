import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { SidenavMenuService } from '@be-lib/services/sidenav-menu/sidenav-menu.service'
import { UIRouterModule } from '@uirouter/angular'
import { ProfileComponent } from './profile.component'
import { ProfileEditComponent } from './profile-edit/profile-edit.component'
import { MatButtonModule } from '@angular/material/button'
import { MatFormFieldModule } from '@angular/material/form-field'
import { FlexModule } from '@angular/flex-layout'
import { MatIconModule } from '@angular/material/icon'
import { FormsModule } from '@angular/forms'
import { MatInputModule } from '@angular/material/input'
import { ViewFrameModule } from '@be-lib/components/view-frame/view-frame.module'

@NgModule({
  declarations: [ProfileComponent, ProfileEditComponent],
  imports: [
    CommonModule,
    UIRouterModule.forChild({
      states: [
        {
          abstract: true,
          name: 'app.private.profile',
          url: '/user',
          component: ProfileComponent,
          redirectTo: 'app.private.profile.edit',
        },
        {
          name: 'app.private.profile.edit',
          url: '/profile',
          component: ProfileEditComponent,
        },
      ],
    }),
    MatButtonModule,
    MatFormFieldModule,
    FlexModule,
    MatIconModule,
    FormsModule,
    MatInputModule,
    ViewFrameModule,
  ],
})
export class ProfileModule {
  constructor(private sidenav: SidenavMenuService) {
    this.sidenav.addItem({
      label: 'Profilo',
      icon: 'person',
      //isVisibleTo: ['admin', 'amministrazione','produzione','magazzino','logistica'],
      route: 'app.private.profile.edit',
    })
  }
}
