import { Injectable, Inject } from '@angular/core'
import { AUTH_INTERCEPTOR_OPTIONS } from './auth-interceptor-options'
import {
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders,
} from '@angular/common/http'
import { Observable } from 'rxjs'
import { tap } from 'rxjs/operators'

@Injectable({
  providedIn: 'root',
})
export class AuthInterceptor implements HttpInterceptor {
  private whitelist: Array<RegExp> = []
  private blacklist: Array<RegExp> = []

  constructor(@Inject(AUTH_INTERCEPTOR_OPTIONS) config: any) {
    this.whitelist = config.whitelist || [new RegExp('*')]
    this.blacklist = config.blacklist || [new RegExp('*/public/*')]
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let cloned: HttpRequest<any>
    if (this.isWhitelisted(request) && !this.isBlacklisted(request)) {
      cloned = request.clone({
        headers: new HttpHeaders({
          Authorization: `Bearer ${localStorage.getItem('token')}`,
          'X-UID': localStorage.getItem('uid') || 'Not-available',
        }),
      })
    } else {
      cloned = request.clone()
    }

    return next.handle(cloned).pipe(
      tap((evt) => {
        if (evt instanceof HttpResponse) {
          const token = evt.headers.get('Authorization')
          const uid = evt.headers.get('X-UID')
          if (
            !!token &&
            token.split(' ')[0] === 'Bearer' &&
            token.split(' ')[1]
          ) {
            localStorage.setItem('token', token.split(' ')[1])
            localStorage.setItem('uid', uid || 'Not-available')
          }
        }
      })
    )
  }

  isWhitelisted(request: HttpRequest<any>): boolean {
    const url = request.url
    return (
      this.whitelist.findIndex((route) =>
        route instanceof RegExp ? route.test(url) : false
      ) > -1
    )
  }

  isBlacklisted(request: HttpRequest<any>): boolean {
    const url = request.url
    return (
      this.blacklist.findIndex((route) =>
        route instanceof RegExp ? route.test(url) : false
      ) > -1
    )
  }
}
