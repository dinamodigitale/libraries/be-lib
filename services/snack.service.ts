import { Injectable } from '@angular/core'
import {
  MatSnackBar,
  MatSnackBarVerticalPosition,
  MatSnackBarHorizontalPosition,
} from '@angular/material/snack-bar'

@Injectable({
  providedIn: 'root',
})
export class SnackService {
  horizontalPosition: MatSnackBarHorizontalPosition = 'right'
  verticalPosition: MatSnackBarVerticalPosition = 'top'
  closeLabel = 'chiudi'

  constructor(private snackBar: MatSnackBar) {}

  message(message: string, action?: string, duration?: number) {
    console.warn(
      'SnackService.message is depricated, please use SnackService.info SnackService.warn SnackService.error instead'
    )
    return this.info(message, action, duration)
  }

  info(message: string, action?: string, duration?: number) {
    return this.snackBar.open(message, action ? action : undefined, {
      duration: duration ? duration : 5000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: ['snackbar-custom', 'snackbar-info'],
    })
  }

  warning = this.warn
  warn(message: string, action?: string, duration?: number) {
    return this.snackBar.open(message, action ? action : this.closeLabel, {
      duration: duration ? duration : 5000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: ['snackbar-custom', 'snackbar-warn'],
    })
  }

  err = this.error
  error(message: string, action?: string, duration?: number) {
    return this.snackBar.open(message, action ? action : this.closeLabel, {
      duration: duration ? duration : 10000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: ['snackbar-custom', 'snackbar-error'],
    })
  }
}
