import { ApiService } from './api.service'
import { Observable } from 'rxjs'
import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root',
})
export abstract class Restify<T> {
  abstract basePath: string

  constructor(protected apiService: ApiService) {}

  index(params = {}): Observable<Array<T>> {
    return this.apiService.get(this.basePath, params)
  }
  get(id, params = {}): Observable<T> {
    return this.apiService.get(`${this.basePath}/${id}`, params)
  }
  destroy(id, params = {}): Observable<T> {
    return this.apiService.delete(`${this.basePath}/${id}`, params)
  }
  update(id, data, params = {}): Observable<T> {
    return this.apiService.patch(`${this.basePath}/${id}`, data, params)
  }
  create(data, params = {}): Observable<T> {
    return this.apiService.post(this.basePath, data, params)
  }
  resource(data, params = {}): Observable<T> {
    return this.apiService.post(`${this.basePath}/resource`, data, params)
  }
  search(keyword: string, limit: number = 5, body = {}): Observable<T> {
    return this.resource(
      { query: { $text: { $search: keyword } }, ...body },
      { params: { limit: limit } }
    )
  }
}
