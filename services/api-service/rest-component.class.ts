import { Observable } from 'rxjs'
import { SnackService } from '../snack.service'
import { HttpHeaders } from '@angular/common/http'
import { StateService } from '@uirouter/core'
import { PageEvent } from '@angular/material/paginator'
import { Injectable } from '@angular/core'

export interface RestBeforeCreate<T> {
  onBeforeCreate(item: T): T
}
export interface RestBeforeUpdate<T> {
  onBeforeUpdate(item: T): T
}
export interface RestBeforeInit {
  onBeforeInit(): void
}

export interface RestAfterCreate<T> {
  onAfterCreate(item: T): void
}
export interface RestAfterUpdate<T> {
  onAfterUpdate(item: T): void
}
export interface RestAfterInit<T> {
  onAfterInit(item: T): void
}

export interface IndexPaginationInterface {
  limit?: number
  page?: number
  totalRecords?: number
  totalPages?: number
  hasNextPage?: boolean
  hasPrevPage?: boolean
  enable?: boolean
}

type IndexApiServiceInterface<T> = (params?: Object) => Observable<T>
type GetApiServiceInterface<T> = (id: string) => Observable<T>
type UpdateApiServiceInterface<T> = (id: string, body: any) => Observable<T>
type DestroyApiServiceInterface<T> = (id: string) => Observable<T>
type CreateApiServiceInterface<T> = (body: any) => Observable<T>
type ResourceApiServiceInterface<T> = (body: any, params?: any) => Observable<T>
type SearchApiServiceInterface<T> = (
  keyword: string,
  limit?: number,
  body?: any
) => Observable<T>

export interface ApiServiceInterface<T> {
  index: IndexApiServiceInterface<Array<T>>
  get: GetApiServiceInterface<T>
  update: UpdateApiServiceInterface<T>
  destroy: DestroyApiServiceInterface<T>
  create: CreateApiServiceInterface<T>
  resource: ResourceApiServiceInterface<T>
  search: SearchApiServiceInterface<T>
}

export interface RestComponentOptions {
  pagination?: IndexPaginationInterface
}

type ViewType = 'create' | 'index' | 'edit' | 'resource'

@Injectable()
export default class RestComponent<T> {
  public headers: HttpHeaders
  public items: Array<T>
  public item: T
  public id: string
  public loading: boolean = true
  public heading: string
  public newButtonText: string

  constructor(
    protected apiService: ApiServiceInterface<T>,
    protected snackService: SnackService,
    protected restType: ViewType,
    protected stateService: StateService,
    protected options?: RestComponentOptions
  ) {}

  ngOnInit(options?: Object) {
    if (this['onBeforeInit']) {
      this['onBeforeInit']()
    }
    if (this.restType === 'edit') {
      this.apiService.get(this.stateService.params.id).subscribe(
        (result) => {
          // this.snackService.info(`Record recuperata con successo`, null, 1000)
          this.item = result
          this.loading = false
          if (this['onAfterInit']) {
            this['onAfterInit'](this.item)
          }
        },
        (err) => {
          console.error(err)
          this.snackService.error(
            err.error.details ||
              err.error.message ||
              `Errore: non è stato possibile recuperare il record ${this.id}`
          )
        }
      )
    } else if (this.restType === 'index') {
      this.index(options)
    } else if (this.restType === 'resource') {
      this.resourceIndex(options)
    } else {
      this.loading = false
    }
  }

  private index(options?: Object) {
    if (
      this.options &&
      this.options.pagination &&
      this.options.pagination.enable
    ) {
      options = Object.assign({}, options, { observe: 'response' })
    }
    this.apiService.index(options).subscribe(
      (result: any) => {
        // this.snackService.info(`Record recuperati con successo`, null, 1000)
        if (
          this.options &&
          this.options.pagination &&
          this.options.pagination.enable
        ) {
          this.headers = result.headers
          this.items = result.body.data
          this.options.pagination = {
            ...result.body.pagination,
          }
        } else {
          this.items = result
        }
        this.loading = false

        if (this['onAfterInit']) {
          this['onAfterInit'](this.items)
        }
      },
      (err) => {
        console.error(err)

        this.snackService.error(
          err.error.details ||
            err.error.message ||
            `Errore: non è stato possibile recuperare i Record`
        )
      }
    )
  }

  update(id: string, data: T): void {
    if (this['onBeforeUpdate']) {
      Object.assign(this.item, this['onBeforeUpdate'](data))
    }

    this.apiService.update(id, data).subscribe(
      (result) => {
        this.snackService.info(`Record aggiornata con successo`)
        this.item = result
        if (this['onAfterUpdate']) {
          this['onAfterUpdate'](data)
        }
      },
      (err) => {
        if (this['onHttpError']) {
          Object.assign(this.item, this['onHttpError'](err))
        }
        if(err.status === 422 && err.error?.errors) {
          err.error.details = `Errore nella compilazione di ${Object.keys(err.error.errors).join(', ')}`
        }
        this.snackService.error(
          err.error.details ||err.error.message || 'Errore: non è stato possibile aggiornare il Record'
        )
      }
    )
  }

  create(data: T): void {
    if (this['onBeforeCreate']) {
      Object.assign(this.item, this['onBeforeCreate'](data))
    }

    this.apiService.create(data).subscribe(
      (result) => {
        this.snackService.info(`Record creato con successo`)
        if (this['onAfterCreate']) {
          this['onAfterCreate'](result)
        }
      },
      (err) => {
        console.error(err)
        if (this['onHttpError']) {
          Object.assign(this.item, this['onHttpError'](err))
        }
        if(err.status === 422 && err.error?.validation?.errors) {
          err.error.details = `Errore nella compilazione di ${Object.keys(err.error?.validation?.errors).join(', ')}`
        }
        this.snackService.error(
          err.error.details ||
            err.error.message ||
            'Errore: Non è stato possibile creare il Record'
        )
      }
    )
  }

  delete(
    id: string,
    options: { softDeleteBy: string } = { softDeleteBy: '_id' }
  ): void {
    this.apiService.destroy(id).subscribe(
      (res) => {
        const i = this.items.findIndex((p) => p[options.softDeleteBy] === id)
        this.items.splice(i, 1)
        this.snackService.info(`Record cancellato con successo`)
      },
      (err) => {
        console.error(err)
        if (this['onHttpError']) {
          Object.assign(this.item, this['onHttpError'](err))
        }
        this.snackService.error(
          err.error.details ||
            err.error.message ||
            `Errore: Record non cancellato`
        )
      }
    )
  }

  paginationChanged(event: PageEvent) {
    this.stateService.go('.', {
      ...this.stateService.params,
      page: event.pageIndex,
      limit: event.pageSize,
    })
  }

  search(event) {
    this.stateService.go('.', { keyword: event })
  }
  private resourceIndex(options?) {
    if (
      this.options &&
      this.options.pagination &&
      this.options.pagination.enable
    ) {
      options = Object.assign({}, options, { observe: 'response' })
    }
    this.apiService.resource(options).subscribe(
      (result: any) => {
        // this.snackService.info(`Record recuperati con successo`, null, 1000)
        if (
          this.options &&
          this.options.pagination &&
          this.options.pagination.enable
        ) {
          this.items = result.data
          this.options.pagination = {
            ...result.pagination,
          }
        } else {
          this.items = result
        }
        this.loading = false

        if (this['onAfterInit']) {
          this['onAfterInit'](this.items)
        }
      },
      (err) => {
        console.error(err)
        this.snackService.error(
          err.error.details ||
            err.error.message ||
            `Errore: non è stato possibile recuperare i Record`
        )
      }
    )
  }
}
