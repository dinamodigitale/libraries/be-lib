export interface SidenavMenuItem {
  icon?: string
  label: string
  route?: string
  params?: Object
  isVisibleTo?: Array<string>
  order?: number
  hidden?: boolean
  clickCB?: Function
  disabled?: boolean
  children?: Array<SidenavMenuItem>
  hideInSearch?: boolean
}
