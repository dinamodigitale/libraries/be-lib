import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { NestedMenuComponent } from './nested-menu/nested-menu.component'
import { IsVisibleToModule } from '@be-lib/directives/is-visible-to/is-visible-to.module'
import { MatIconModule } from '@angular/material/icon'
import { MatListModule } from '@angular/material/list'

@NgModule({
  declarations: [NestedMenuComponent],
  imports: [CommonModule, IsVisibleToModule, MatIconModule, MatListModule],
  exports: [NestedMenuComponent],
})
export class SidenavMenuModule {}
