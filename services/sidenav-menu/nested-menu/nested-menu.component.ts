import { Component, HostBinding, Input, OnInit } from '@angular/core'
import { animate, state, style, transition, trigger } from '@angular/animations'
import { SidenavMenuItem } from '../sidenav-menu-item'
import { StateService } from '@uirouter/core'

@Component({
  selector: 'dbe-nested-menu',
  templateUrl: './nested-menu.component.html',
  styleUrls: ['./nested-menu.component.scss'],
  animations: [
    trigger('indicatorRotate', [
      state('collapsed', style({ transform: 'rotate(0deg)' })),
      state('expanded', style({ transform: 'rotate(180deg)' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
      ),
    ]),
  ],
})
export class NestedMenuComponent implements OnInit {
  expanded: boolean
  @HostBinding('attr.aria-expanded') ariaExpanded
  @Input() public item: SidenavMenuItem
  @Input() public depth: number

  constructor(public stateService: StateService) {
    this.ariaExpanded = this.expanded
    if (this.depth === undefined) {
      this.depth = 0
    }
  }

  onItemSelected(item: SidenavMenuItem) {
    if (!item.children || !item.children.length) {
      this.stateService.go(item.route, item.params ? item.params : {}, {reload: true})
    }
    if (item.children && item.children.length) {
      this.expanded = !this.expanded
    }
  }
  ngOnInit() {}
}
