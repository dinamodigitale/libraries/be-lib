export interface PartialInterface {
  [x: string]: any
}
