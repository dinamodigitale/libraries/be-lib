import { Injectable } from '@angular/core'
import { ApiService } from '../api-service/api.service'
import { Restify } from '../api-service/restify.class'
import { PartialInterface } from './partial-interface'

@Injectable({
  providedIn: 'root',
})
export class PartialService extends Restify<PartialInterface> {
  basePath = '/backend/partials'
  constructor(protected apiService: ApiService) {
    super(apiService)
  }

  index() {
    return this.apiService.get(this.basePath, {
      params: {
        limit: 1000000,
      },
    })
  }
}
