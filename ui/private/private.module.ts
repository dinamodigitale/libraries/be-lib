import { NgModule, Injector } from '@angular/core'
import { PrivateComponent } from './private.component'
import { UIRouterModule, UIRouter, RejectType } from '@uirouter/angular'

import { AuthService } from '@be-lib/services/auth-service.service'
import { SidenavMenuModule } from '@be-lib/services/sidenav-menu/sidenav-menu.module'
import { MatSidenavModule } from '@angular/material/sidenav'
import { MatListModule } from '@angular/material/list'
import { MatToolbarModule } from '@angular/material/toolbar'

import { MatIconModule } from '@angular/material/icon'
import { MatMenuModule } from '@angular/material/menu'
import { MatButtonModule } from '@angular/material/button'
import { FlexLayoutModule } from '@angular/flex-layout'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatAutocompleteModule } from '@angular/material/autocomplete'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { MatInputModule } from '@angular/material/input'

export function authConfig(router: UIRouter, injector: Injector) {
  const authService = injector.get(AuthService)

  router.transitionService.onStart(
    {
      to: '**.private.**',
    },
    (trans) => {
      return authService.isLoggedIn().toPromise()
    }
  )

  router.transitionService.onError(
    {
      to: '**.private.**',
    },
    (trans) => {
      const rejection = trans.error()
      if (
        rejection.type === RejectType.ERROR &&
        rejection.detail &&
        rejection.detail.status &&
        rejection.detail.status === 401
      ) {
        // === 6
        console.warn(
          '401 unauthorized response from the server, redirecting to login'
        )
        authService.removeUserInfo()
        trans.router.stateService.go('app.public.login')
      } else {
        console.log('Transition error', rejection)
        return true
      }
    }
  )
}

@NgModule({
  imports: [
    BrowserModule,
    UIRouterModule.forChild({
      states: [
        {
          name: 'app.private',
          url: '/private',
          component: PrivateComponent,
        },
      ],
      config: authConfig,
    }),
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    SidenavMenuModule,
  ],
  declarations: [PrivateComponent],
  providers: [],
})
export class PrivateModule {}
