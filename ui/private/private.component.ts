import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core'
import { SidenavMenuService } from '@be-lib/services/sidenav-menu/sidenav-menu.service'
import { SidenavMenuItem } from '@be-lib/services/sidenav-menu/sidenav-menu-item'
import { StateService, TransitionService } from '@uirouter/core'
import { AuthService, User } from '@be-lib/services/auth-service.service'
import { FormControl } from '@angular/forms'
import { Observable } from 'rxjs'
import { map, startWith } from 'rxjs/operators'
import { Title } from '@angular/platform-browser'

@Component({
  selector: 'dinamo-private',
  templateUrl: './private.component.html',
  styleUrls: ['./private.component.scss'],
})
export class PrivateComponent {
  @ViewChild('snav') snav
  @ViewChild('autocompleteInput') input: ElementRef<HTMLInputElement>

  public items: Array<SidenavMenuItem> = []
  public isActive: Boolean = true
  public userId
  mode: string

  appName: string = 'Backend'

  autocompleteItems: SidenavMenuItem[] = []

  sideNavOpen = false

  currentUser: User

  autocompleteControl = new FormControl()
  filteredOptions: Observable<SidenavMenuItem[]>

  constructor(
    private authService: AuthService,
    private state: StateService,
    private sidenav: SidenavMenuService,
    private stateService: StateService,
    private title: Title,
    transitionService: TransitionService,
  ) {
    this.items = this.sidenav.items
    this.autocompleteItems = JSON.parse(JSON.stringify(this.sidenav.items))
    this.manageChildren(this.items)
    this.initAutocomplete()
    transitionService.onStart({ from: '**', to: '**' }, () => {
      this.initAutocomplete()
      this.sideNavOpen = false
    })
    this.appName = this.title.getTitle()
  }

  private manageChildren(items: any[]) {
    items.forEach(x => {
      if (x.children && x.children.length) {
        this.autocompleteItems.push(...x.children)
        this.manageChildren(x.children)
      }
    })
  }

  private initAutocomplete() {
    if (this.input && this.input.nativeElement) {
      this.input.nativeElement.blur()
    }
    this.autocompleteControl.reset()
    this.filteredOptions = this.autocompleteControl.valueChanges.pipe(
      startWith(''),
      map((value) => this._filter(value))
    )
  }

  private _filter(value: string): SidenavMenuItem[] {
    if (!value) {
    }
    return this.autocompleteItems.filter((item) => {
      if (value) {
        return (
          item.label.toLowerCase().includes(value.toLowerCase()) &&
          item.route !== this.stateService.current.name &&
          !item.hideInSearch
        )
      }
      return item.route !== this.stateService.current.name && !item.hideInSearch
    })
  }

  logout() {
    this.authService.logout().subscribe(
      () => this.state.go('app.public.login'),
      () => console.error
    )
  }

  get userInfo(): any {
    return this.authService.getUserInfo()
  }

  navigate(item: SidenavMenuItem) {
    this.stateService.go(item.route, item.params || {})
  }

  changeMode() {
    document.getElementsByTagName('body')[0].classList.toggle(`dark`)
  }
}
