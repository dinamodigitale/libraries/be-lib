import { NgModule } from '@angular/core'

import { UIRouterModule } from '@uirouter/angular'
import { BrowserModule } from '@angular/platform-browser'

import { PublicComponent } from './public.component'
import { PageNotFoundModule } from './page-not-found/page-not-found.module'
import { LoginModule } from './login/login.module'
import { FlexLayoutModule } from '@angular/flex-layout'
import { ForgotPasswordModule } from './forgot-password/forgot-password.module'
import { ResetPasswordModule } from './reset-password/reset-password.module'

@NgModule({
  imports: [
    FlexLayoutModule,
    UIRouterModule.forChild({
      states: [
        {
          name: 'app.public',
          url: '',
          component: PublicComponent,
        },
      ],
    }),
    BrowserModule,
    PageNotFoundModule,
    LoginModule,
    ForgotPasswordModule,
    ResetPasswordModule
  ],
  declarations: [PublicComponent],
})
export class PublicModule {}
