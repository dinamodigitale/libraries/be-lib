import { Component, OnInit } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { FormBuilder } from '@angular/forms'
import { Validators } from '@angular/forms'
import { MatSnackBar } from '@angular/material/snack-bar'
import { AuthService } from '@be-lib/services/auth-service.service'
import { StateService } from '@uirouter/core'

@Component({
  selector: 'dinamo-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  form: FormGroup
  errors: Array<any> = []
  currentLocale: any

  constructor(
    private state: StateService,
    private fb: FormBuilder,
    private authService: AuthService,
    private snackbar: MatSnackBar
  ) {
    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    })

    this.authService.isLoggedIn().subscribe(
      () => {
        console.log('Authenticated, redirect to dashboard')
        this.state.go('app.private.dashboard')
      },
      () => {
        console.warn('Not authenticated, need to login')
      }
    )
  }

  setLocale(locale) {
    this.currentLocale = locale
  }

  login() {
    const val = this.form.value

    if (val.email && val.password) {
      this.authService.login(val.email, val.password).subscribe(
        (res) => {
          window.localStorage.privacy = 'true'
          this.state.go('app.private.dashboard')
        },
        (err) => {
          console.error(err)
          this.snackbar.open(
            err.error.description || 'Authentication failed',
            '',
            { duration: 5000 }
          )
        }
      )
    }
  }
}
