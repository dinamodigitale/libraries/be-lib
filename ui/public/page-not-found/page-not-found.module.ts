import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { BrowserModule } from '@angular/platform-browser'
import { PageNotFoundComponent } from './page-not-found.component'
import { UIRouterModule } from '@uirouter/angular'

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,

    UIRouterModule.forChild({
      states: [
        {
          name: 'app.public.404',
          url: '/page-not-found',
          component: PageNotFoundComponent,
        },
      ],
    }),
  ],
  declarations: [PageNotFoundComponent],
})
export class PageNotFoundModule {}
