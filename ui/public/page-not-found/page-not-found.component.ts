import { Component } from '@angular/core'

@Component({
  selector: 'dinamo-page-not-found',
  templateUrl: 'page-not-found.html',
})
export class PageNotFoundComponent {}
