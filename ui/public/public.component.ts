import { Component } from '@angular/core'

@Component({
  selector: 'dinamo-public',
  templateUrl: 'public.component.html',
  styleUrls: ['public.component.scss'],
})
export class PublicComponent {
  constructor() {}
}
