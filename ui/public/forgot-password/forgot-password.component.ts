import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { AuthService } from '@be-lib/services/auth-service.service'
import { MatSnackBar } from '@angular/material/snack-bar'
import { StateService } from '@uirouter/core'

@Component({
  selector: 'dbe-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit {
  form: FormGroup
  errors: Array<any> = []

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private snackbar: MatSnackBar,
    private stateService: StateService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      email: ['', Validators.required],
    })
  }

  sendForgot() {
    if (this.form.value.email) {
      this.authService.generateToken(this.form.value.email).subscribe(
        (res) => {
          this.snackbar.open('Email di reset password inviata!')
          this.stateService.go('app.public.login')
        },
        (err) => {
          console.log(err)
          this.snackbar.open('Si è verificato un problema..')
        }
      )
    }
  }
}
