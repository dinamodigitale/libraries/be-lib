import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { BrowserModule } from '@angular/platform-browser'
import { UIRouterModule } from '@uirouter/angular'
import { ForgotPasswordComponent } from './forgot-password.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { MatSnackBarModule } from '@angular/material/snack-bar'
import { MatCardModule } from '@angular/material/card'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input'
import { MatButtonModule } from '@angular/material/button'
import { FlexLayoutModule } from '@angular/flex-layout'
import { AuthService } from '@be-lib/services/auth-service.service'

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,

    UIRouterModule.forChild({
      states: [
        {
          name: 'app.public.forgot-password',
          url: '/forgot-password',
          component: ForgotPasswordComponent,
        },
      ],
    }),
    ReactiveFormsModule,
    FormsModule,
    MatSnackBarModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    FlexLayoutModule,
  ],
  declarations: [ForgotPasswordComponent],
  providers: [AuthService],
})
export class ForgotPasswordModule {}
