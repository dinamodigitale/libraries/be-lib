import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import {
  DialogChipsComponent,
  ChipDirective,
  DialogDirective,
  AutocompleteOptionDirective,
} from './dialog-chips.component'
import { DialogForDialogChipsComponent } from './dialog-for-dialog-chips/dialog-for-dialog-chips.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatChipsModule } from '@angular/material/chips'
import { MatAutocompleteModule } from '@angular/material/autocomplete'
import { MatIconModule } from '@angular/material/icon'

@NgModule({
  declarations: [
    DialogChipsComponent,
    DialogForDialogChipsComponent,
    ChipDirective,
    DialogDirective,
    AutocompleteOptionDirective,
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatChipsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatIconModule,
  ],
  exports: [
    DialogChipsComponent,
    ChipDirective,
    DialogDirective,
    AutocompleteOptionDirective,
  ],
  entryComponents: [DialogForDialogChipsComponent],
})
export class DialogChipsModule {}
