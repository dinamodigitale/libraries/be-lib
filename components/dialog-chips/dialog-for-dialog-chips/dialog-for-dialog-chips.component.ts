import { Component, Inject } from '@angular/core'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'

@Component({
  selector: 'dbe-dialog-for-dialog-chips',
  templateUrl: './dialog-for-dialog-chips.component.html',
  styleUrls: ['./dialog-for-dialog-chips.component.scss'],
})
export class DialogForDialogChipsComponent {
  abc: any
  template: any
  title: string

  tmpElement: any
  element: any

  constructor(
    public dialogRef: MatDialogRef<DialogForDialogChipsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.template = data.template
    this.title = data.title

    try {
      this.tmpElement = JSON.parse(JSON.stringify(data.element))
    } catch (e) {
      this.tmpElement = data.element
    }
    this.element = data.element
  }

  onNoClick(): void {
    this.dialogRef.close(null)
  }

  save() {
    this.dialogRef.close(this.element)
  }
}
