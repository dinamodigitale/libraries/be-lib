import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  ViewChildren,
  Directive,
  TemplateRef,
  ContentChild,
} from '@angular/core'
import { ENTER, COMMA } from '@angular/cdk/keycodes'
import { FormControl } from '@angular/forms'
import { Observable } from 'rxjs'
import { MatDialog } from '@angular/material/dialog'
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete'
import { MatChipInputEvent } from '@angular/material/chips'
import { config } from '@app/environments/environment'
import { startWith, map } from 'rxjs/operators'
import { DialogForDialogChipsComponent } from './dialog-for-dialog-chips/dialog-for-dialog-chips.component'

@Directive({
  selector: '[dinamoChip]',
})
export class ChipDirective {}

@Directive({
  selector: '[dinamoAutocompleteOption]',
})
export class AutocompleteOptionDirective {}

@Directive({
  selector: '[dinamoDialogContent]',
})
export class DialogDirective {}

@Component({
  selector: 'dbe-dialog-chips',
  templateUrl: './dialog-chips.component.html',
  styleUrls: ['./dialog-chips.component.scss'],
})
export class DialogChipsComponent implements OnInit {
  visible = true
  selectable = true
  removable = true
  addOnBlur = false

  availableLocales: Array<string>

  separatorKeysCodes: number[] = [ENTER, COMMA]
  elementCtrl = new FormControl()

  @ContentChild(ChipDirective, { read: TemplateRef }) chipTemplate
  @ContentChild(DialogDirective, { read: TemplateRef }) dialogTemplate
  @ContentChild(AutocompleteOptionDirective, { read: TemplateRef })
  autocompleteOptionTemplate

  filteredElements: Observable<string[]>

  @Input() dialogTitle: string
  @Input() newElementsAvailable = false
  @Input() availableElements: Array<string> = []

  @Input() idIn: string = '$ROOT'
  @Input() searchBy: string = '$ROOT'

  @Input() selectedElements: Array<string> = []
  @Output() selectedElementsChange: EventEmitter<any> = new EventEmitter<any>()

  // tslint:disable-next-line:no-inferrable-types
  @Input() placeholder: string = 'Element'
  @Input() disableDialog: boolean = false

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onNewElement: EventEmitter<any> = new EventEmitter<any>()
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onSelectElement: EventEmitter<any> = new EventEmitter<any>()
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onUpdateElement: EventEmitter<any> = new EventEmitter<any>()
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onDeleteElement: EventEmitter<any> = new EventEmitter<any>()

  get allElements() {
    return this.availableElements.filter((ae) => {
      return !this.selectedElements.find((se) => {
        return (
          this.getObjectValue(ae, this.idIn) ===
          this.getObjectValue(se, this.idIn)
        )
      })
    })
  }

  @ViewChild('elementInput') elementInput: ElementRef
  @ViewChildren('chip') chip: ElementRef

  constructor(public dialog: MatDialog) {}

  ngOnInit() {
    this.assignFilteredElements()
  }

  labelize(element) {
    if (typeof element === typeof 'string') {
      return element
    } else {
      return element[config.i18n.defaultLocale]
    }
  }

  private assignFilteredElements() {
    this.filteredElements = this.elementCtrl.valueChanges.pipe(
      startWith(null),
      map((element: string | null) => {
        return element ? this._filter(element) : this.allElements.slice()
      })
    )
  }

  openModal(element: any) {
    if (this.disableDialog) {
      return false
    }

    const self = this
    const oldElement = JSON.parse(JSON.stringify(element))

    const dialogRef = this.dialog.open(DialogForDialogChipsComponent, {
      data: {
        template: this.dialogTemplate,
        title: this.dialogTitle,
        element,
      },
    })

    dialogRef.afterClosed().subscribe((result) => {
      if (!result) {
        const i = self.selectedElements.findIndex((e: any) => {
          return (
            self.getObjectValue(e, self.idIn) ===
            self.getObjectValue(element, self.idIn)
          )
        })
        self.selectedElements[i] = oldElement
      }
    })
  }

  remove(element: string): void {
    const index = this.selectedElements.indexOf(element)

    if (index >= 0) {
      this.selectedElements.splice(index, 1)
      this.assignFilteredElements()
      this.onDeleteElement.emit(element)
    }

    this.selectedElementsChange.emit(this.selectedElements)
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    const selected = event.option.value

    this.onSelectElement.emit(selected)

    this.selectedElements.push(selected)
    this.elementInput.nativeElement.value = ''
    this.elementCtrl.setValue(null)

    if (this.dialogTemplate) {
      this.openModal(selected)
    } else {
      this.selectedElementsChange.emit(this.selectedElements)
    }
  }

  entered(event: MatChipInputEvent): void {
    // if (this.newElementsAvailable) {
    //   const tag = event.value.trim();
    //   if (tag) {
    //     if (this._filter(tag).length) {
    //       this.onNewElement.emit(tag);
    //     } else {
    //       this.openModal(tag, 'onNewElement');
    //       this.availableElements.push(tag);
    //     }
    //     this.assignFilteredElements();
    //     this.elementInput.nativeElement.value = '';
    //     this.elementCtrl.setValue(null);
    //     this.selectedElements.push(tag);
    //     this.selectedElementsChange.emit(this.selectedElements);
    //   }
    // }
  }

  private _filter(filterValue: string): Array<string> {
    return this.allElements.filter((element) => {
      let value = ''
      try {
        value = this.getObjectValue(element, this.searchBy).toLowerCase().trim()
      } catch (e) {}

      let fv = ''
      try {
        fv = filterValue.toLowerCase().trim()
      } catch (e) {}

      return value.indexOf(fv) >= 0
    })
  }

  getColor(element) {
    if (typeof element === typeof {} && !Array.isArray(element)) {
      if (!element[config.i18n.defaultLocale]) {
        return 'languages-in-error'
      } else {
        return !this.availableLocales.find((l) => {
          return !element[l]
        })
          ? 'languages-complete'
          : 'languages-non-complete'
      }
    } else {
      return null
    }
  }

  private getObjectValue(element: any, key: string): any {
    if (key === '$ROOT') {
      return element
    }

    const splittedKey = key.split('.')
    const searchingKey = splittedKey[0]
    splittedKey.shift()

    element = element[searchingKey]

    if (typeof element === typeof undefined) {
      return undefined
    } else if (typeof element !== typeof undefined && !splittedKey.length) {
      return element
    } else {
      return this.getObjectValue(element, splittedKey.join('.'))
    }
  }
}
