import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import {
  GalleryComponent,
  DinamoGalleryCustomFieldsDirective,
} from './gallery.component'
import { DinamoResourceModule } from '../dinamo-resource/dinamo-resource.module'

@NgModule({
  declarations: [GalleryComponent, DinamoGalleryCustomFieldsDirective],
  imports: [CommonModule, DinamoResourceModule],
  exports: [GalleryComponent, DinamoGalleryCustomFieldsDirective],
})
export class GalleryModule {}
