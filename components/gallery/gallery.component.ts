import {
  Component,
  Input,
  EventEmitter,
  Output,
  Directive,
  ContentChild,
  TemplateRef,
  OnInit,
} from '@angular/core'

@Directive({
  selector: '[dinamoGalleryCustomFields]',
})
export class DinamoGalleryCustomFieldsDirective {}

@Component({
  selector: 'dbe-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
})
export class GalleryComponent implements OnInit {
  @Input() model
  @Output() modelChange: EventEmitter<any> = new EventEmitter<any>()

  @ContentChild(DinamoGalleryCustomFieldsDirective, { read: TemplateRef })
  galleryCustomFieldsTemplate

  constructor() {}

  ngOnInit() {
    console.log(this.galleryCustomFieldsTemplate)
  }

  addGalleryImage() {
    if (this.model) {
      this.model.push({})
    } else {
      this.model = [{}]
    }
    this.modelChange.emit(this.model)
  }

  moveGalleryElement(from: any, where: string): boolean {
    let to
    if (where === 'prev') {
      to = parseInt(from, 10) - 1
    } else if (where === 'next') {
      to = parseInt(from, 10) + 1
    } else {
      return false
    }
    this.model.splice(to, 0, this.model.splice(from, 1)[0])
    this.modelChange.emit(this.model)
    return true
  }

  removeGalleryElement(index: any) {
    index = parseInt(index, 10)
    this.model.splice(index, 1)
    this.modelChange.emit(this.model)
  }
}
