import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ProSelectComponent } from './pro-select.component'
import { FormsModule } from '@angular/forms'
import { ProSelectActiveDirective } from './pro-select-active.directive'
import { ProSelectOptionDirective } from './pro-select-option.directive'

@NgModule({
  declarations: [
    ProSelectComponent,
    ProSelectActiveDirective,
    ProSelectOptionDirective,
  ],
  imports: [CommonModule, FormsModule],
  exports: [
    ProSelectComponent,
    ProSelectActiveDirective,
    ProSelectOptionDirective,
  ],
})
export class ProSelectModule {}
