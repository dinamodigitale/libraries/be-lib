import { EventEmitter } from '@angular/core';
import { Observable, empty } from 'rxjs';

export abstract class ComponentsLoaderApi extends EventEmitter<any> {

  abstract type: String;
  abstract initialize(): Observable<any>;
  save(partial: any): Observable<any> { return empty(); }
  delete(partial: any): Observable<any> { return empty(); }
  toggle(partial: any): Observable<any> { return empty(); }
  position(partial: any): Observable<any> { return empty(); }

}
