import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { entryComponents } from '@app/app/components/partials'
import { ApiServiceInterface } from '@be-lib/services/api-service/rest-component.class'
import { PartialService } from '@be-lib/services/partial-service/partial.service'
import { SnackService } from '@be-lib/services/snack.service'
import { AuthService } from '@be-lib/services/auth-service.service'

@Component({
  selector: 'dbe-components-loader',
  templateUrl: './components-loader.component.html',
  styleUrls: ['./components-loader.component.scss'],
  entryComponents,
})
export class ComponentsLoaderComponent implements OnInit {
  softDelete = {
    yesButton: 'SI',
    noButton: 'NO',
    title: 'Rimuovi dalla lista?',
    message:
      'Vuoi rimuovere il partial dalla lista? Potrai continuare ad utilizzarlo dalla lista "Tutti i Partial"',
  }

  hardDelete = {
    yesButton: 'SI',
    noButton: 'NO',
    title: 'Elimina il Partial?',
    message: 'Vuoi eliminare definitivamente il partial?',
  }

  @Input() apiService: ApiServiceInterface<any>
  @Input() entity: any
  @Input() componentsToShow: string[] = []

  // @Input() apiClass: ComponentsLoaderApi

  @Input() showAvailablePartials = false

  /**
   * Array of partials ID
   */
  @Input() components = []
  /**
   * Existing and associated partials
   */
  @Input() selectedComponentsTitle: String = 'Componenti Inseriti'
  /**
   * Existing and associated partials
   */
  @Input() availableComponentsTitle: String = 'Componenti Disponibili'
  /**
   * All the available partials
   */
  availablePartials = []
  /**
   * The post processed available partials (availablePartials - components)
   */
  availableComponents: Array<any> = []
  /**
   * An array of all the createable components
   */
  availableNewComponents = []
  availablePartialList

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onSave: EventEmitter<any> = new EventEmitter()

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onDelete: EventEmitter<any> = new EventEmitter()

  flexDimenstions = '100'

  partials: any[] = []

  constructor(
    private partialService: PartialService,
    private snackService: SnackService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.initAvailableNewComponents()
    this.partialService
      .resource(
        { query: { _id: this.components } },
        { params: { limit: 1000 } }
      )
      .subscribe((res) => {
        // let r
        // this.components.map((item, i) => {
        //   r = res.data.some(p => p._id.includes(item))
        //   // console.log(r, i, item)
        //   if (!r) {
        //     this.components.splice(i, 1)
        //   }
        // })
        this.partials = this.components
          .map((item) => {
            const tmp = item?._id ? item._id : item
            return res.data.find((partial) => {
              return partial._id === tmp
            })
          })
          .filter((c) => !!c)
      })

    this.availableNewComponents = entryComponents
      .filter((x) =>
        this.componentsToShow.length > 0
          ? this.componentsToShow?.includes(x.constructorName)
          : true
      )
      .map((component) => ({
        type: component.constructorName,
        payload: {},
        // type: this.apiClass.type,
        name: component.name,
        label: component.label,
      }))
  }

  private initAvailableNewComponents() {
    this.availableNewComponents = entryComponents
      .filter((x) =>
        this.componentsToShow.length > 0
          ? this.componentsToShow?.includes(x.constructorName)
          : true
      )
      .map((component) => ({
        type: component.constructorName,
        payload: {},
        name: component.name,
        label: component.label,
      }))
  }

  get existingPartials() {
    /**
     * this.availableComponents potrebbero arrivare dopo che il template renderizza
     * la var existingPartials, quindi darebbe errore
     */
    try {
      return this.availableComponents
        .filter((partial) => !this.components.includes(partial['_id']))
        .map((p) => p._id)
    } catch (e) {
      return []
    }
  }
  // }

  dragged($event) {
    if (this.entity && this.apiService) {
      this.apiService.update(this.entity, { partials: $event }).subscribe(
        (res) => {
          this.snackService.info('Ordine partial aggiornato')
        },
        (e) => {
          this.snackService.error(e)
        }
      )
    } else {
      console.log('missing entity or apiService')
    }
  }

  public getPartial(id) {
    return this.partials.find((p) => p._id === id) || {}
  }

  public getPartialProperty(id, property) {
    return (
      (this.availablePartials.find((p) => p._id === id) || {})[property] ||
      'n.d.'
    )
  }

  public save(data) {
    let subscription
    let newPartial = false
    if (data._id) {
      subscription = this.partialService.update(data._id, data)
    } else {
      subscription = this.partialService.create(data)
      newPartial = true
    }
    subscription.subscribe((res) => {
      if (data._id) {
        const index = this.partials.findIndex((e) => e._id === data._id)
        if (index >= 0) {
          this.partials[index] = res
          this.snackService.info('Partial aggiornato')
        }
      } else {
        this.partials.unshift(res)
        this.snackService.info('Partial creato')
        this.updateRootObject()
      }
      this.onSave.emit({ partial: res, newPartial })
      this.initAvailableNewComponents()
    })
  }

  updateRootObject() {
    this.apiService
      .update(this.entity, {
        partials: [...this.partials.map((p) => p._id)],
      })
      .subscribe()
  }

  delete($event) {
    this.partialService.destroy(this.partials[$event]._id).subscribe(
      (res) => {
        this.partials.splice($event, 1)
        this.updateRootObject()
        this.snackService.info('Partial rimosso')
        this.onDelete.emit($event)
      },
      (e) => {
        this.snackService.error(e)
      }
    )
  }
}
