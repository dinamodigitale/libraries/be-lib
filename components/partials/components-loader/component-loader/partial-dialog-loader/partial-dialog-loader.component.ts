import {
  Component,
  Inject,
  ViewChild,
  ComponentFactoryResolver,
  ViewContainerRef,
  ComponentRef,
  OnInit,
  OnDestroy
} from '@angular/core'
import { entryComponents } from '@app/app/components/partials'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'
import { DinamoPartial } from './dinamo-partial.interface'
import { AuthService } from '@be-lib/services/auth-service.service'

@Component({
  selector: 'dbe-partial-dialog-loader',
  templateUrl: './partial-dialog-loader.component.html',
  styleUrls: ['./partial-dialog-loader.component.scss'],
  entryComponents
})
export class DinamoPartialDialogComponent implements OnInit, OnDestroy {
  @ViewChild('target', { read: ViewContainerRef, static: true })
  vcRef: ViewContainerRef
  // tslint:disable-next-line:no-output-on-prefix

  componentRef: ComponentRef<any>
  partial: DinamoPartial = {
    name: '',
    user: '',
    label: '',
    type: '',
    payload: {},
    // palette: {},
    published: true
  }

  constructor(
    public dialogRef: MatDialogRef<DinamoPartialDialogComponent>,
    private resolver: ComponentFactoryResolver,
    private authService: AuthService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    const factory = this.resolver.resolveComponentFactory(this.data.component)
    this.componentRef = this.vcRef.createComponent(factory)
    if (!this.data.resources) {
      this.data.resources = []
    }
    this.partial = Object.assign(
      { payload: {}, resources: this.data.resources },
      this.data.data
    )
    Object.assign(this.componentRef.instance, this.partial)
  }

  onNoClick(): void {
    this.dialogRef.close()
  }

  ngOnDestroy() {
    if (this.componentRef) {
      this.componentRef.destroy()
    }
  }

  save() {
    let partial = {}
    if (this.componentRef.instance._id) {
      partial = this.partial
    } else {
      partial = Object.assign(this.partial, {
        user: this.authService.getUserInfo()['_id']
      })
    }
    this.dialogRef.close(partial)
  }

  // changePalette($event) {
  //   this.partial.palette = $event
  // }

  changePublished($event) {
    this.partial.published = $event.checked
  }
}
