export interface DinamoPartial {
  name: String;
  user: String;
  label: String;
  type: String;
  payload: Object;
  // palette: Object;
  published: boolean
}
