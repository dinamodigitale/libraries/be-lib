import { Directive, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { entryComponents } from '@app/app/components/partials'
import { MatDialog } from '@angular/material/dialog';
import { DinamoPartialDialogComponent } from './partial-dialog-loader/partial-dialog-loader.component';

@Directive({
  selector: '[dbeComponentLoader]'
})
export class ComponentLoaderDirective {


  @Input() component;
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onClick: EventEmitter<any> = new EventEmitter();
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onSave: EventEmitter<any> = new EventEmitter();
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onClose: EventEmitter<any> = new EventEmitter();

  constructor(
    public dialog: MatDialog,
    private domElement: ElementRef
  ) {
    this.domElement.nativeElement.addEventListener('click', () => {
      this.openDialog();
    });
  }
  private findComponent(componentName) {
    return entryComponents.find(component => component.constructorName === componentName);
  }

  openDialog(): void {
    // debugger
    const dialogRef = this.dialog.open(DinamoPartialDialogComponent, {
      width: '70vw',
      maxHeight: '100vh',
      data: {
        component: this.findComponent(this.component.type || this.component.name),
        data: this.component
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        this.onClose.emit(null);
      } else {
        this.onSave.emit(result);
      }
    });
    this.onClick.emit(this.component.name);
  }
}
