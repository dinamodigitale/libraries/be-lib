import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ComponentsLoaderComponent } from './components-loader/components-loader.component'
import { DinamoPartialDialogComponent } from './components-loader/component-loader/partial-dialog-loader/partial-dialog-loader.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { ComponentLoaderDirective } from './components-loader/component-loader/component-loader.directive'
import { ConfirmModule } from '@be-lib/directives/confirm/confirm.module'
import { I18nInputModule } from '@be-lib/components/i18n-input/i18n-input.module'
import { MatIconModule } from '@angular/material/icon'
import { MatMenuModule } from '@angular/material/menu'
import { DragDropModule } from '@angular/cdk/drag-drop'
import { DinamoDraggerModule } from '../dinamo-dragger/dinamo-dragger.module'
import { FlexLayoutModule } from '@angular/flex-layout'
import { MatInputModule } from '@angular/material/input'
import { MatButtonModule } from '@angular/material/button'
import { MatDialogModule } from '@angular/material/dialog'
import { MatDividerModule } from '@angular/material/divider'
import { DinamoResourceModule } from '../dinamo-resource/dinamo-resource.module'
import { MatRadioModule } from '@angular/material/radio'
import { MatCheckboxModule } from '@angular/material/checkbox'
import { MatSelectModule } from '@angular/material/select'
import {
  MatAutocomplete,
  MatAutocompleteModule,
} from '@angular/material/autocomplete'
import { MatChipsModule } from '@angular/material/chips'
import { MatExpansionModule } from '@angular/material/expansion'
import { MatCardModule } from '@angular/material/card'
import { MatSlideToggleModule } from '@angular/material/slide-toggle'
import { MatTabsModule } from '@angular/material/tabs'
import { PartialsListModule } from '@app/app/components/partials/partials-list.module'
// import { entryComponents } from '@app/app/components/partials'

@NgModule({
  declarations: [
    ComponentsLoaderComponent,
    DinamoPartialDialogComponent,
    ComponentLoaderDirective,
    // partials components declarations
    // ...entryComponents,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DinamoResourceModule,
    ConfirmModule,
    I18nInputModule,
    MatIconModule,
    MatMenuModule,
    DragDropModule,
    DinamoDraggerModule,
    FlexLayoutModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatDividerModule,
    MatRadioModule,
    MatCheckboxModule,
    MatSelectModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatExpansionModule,
    MatCardModule,
    MatSlideToggleModule,
    MatTabsModule,
    PartialsListModule,
  ],
  exports: [ComponentsLoaderComponent, ComponentLoaderDirective],
  entryComponents: [DinamoPartialDialogComponent],
})
export class PartialModule {}
