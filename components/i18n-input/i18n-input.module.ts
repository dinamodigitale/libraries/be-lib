import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { I18nInputComponent } from './i18n-input.component'
import { FormsModule } from '@angular/forms'
import { DinamoHtmlEditorModule } from '../dinamo-html-editor/dinamo-html-editor.module'
import { MatInputModule } from '@angular/material/input'
import { FlexLayoutModule } from '@angular/flex-layout'

@NgModule({
  declarations: [I18nInputComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatInputModule,
    DinamoHtmlEditorModule,
    FlexLayoutModule,
  ],
  exports: [I18nInputComponent],
})
export class I18nInputModule {}
