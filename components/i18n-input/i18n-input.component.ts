import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  AfterViewInit,
  ElementRef,
  ViewChildren,
  HostListener,
} from '@angular/core'
import { config } from '@app/environments/environment'
import { nextTick } from 'process'

@Component({
  selector: 'dbe-i18n-input',
  templateUrl: './i18n-input.component.html',
  styleUrls: ['./i18n-input.component.scss'],
})
export class I18nInputComponent implements OnInit, AfterViewInit {
  @Input() model: any = {}
  @Input() disabled = false
  @Output() modelChange: EventEmitter<any> = new EventEmitter<any>()
  @Input() placeholder: String
  @Input() availableLocales: Array<String> = []
  @Input() defaultLocale: String = ''
  @Input() type: String = 'input'
  @ViewChildren('refs') refs: Array<ElementRef>

  public selectedLocale: String = ''

  constructor() {}

  ngOnInit() {
    this.type = this.type.trim().toLowerCase()
    if (
      this.type !== 'input' &&
      this.type !== 'textarea' &&
      this.type !== 'html'
    ) {
      throw new Error(
        `must choose type for dinamo-i18n-input between [input, textarea, html] ${this.type} doesnt exists`
      )
    }

    if (!this.availableLocales.length) {
      this.availableLocales = config.i18n.availableLocales
    }
    if (!this.defaultLocale) {
      this.defaultLocale = config.i18n.defaultLocale
    }
    this.selectedLocale = this.defaultLocale
  }

  ngAfterViewInit() {
    setTimeout(() => {
      if (!this.model) {
        this.model = {}
      } else if (typeof this.model === 'string') {
        const model = this.model
        this.model = {}
        this.availableLocales.forEach((l: any) => (this.model[l] = model))
      }
      Object.assign(this.model, { _i18n: true })
      this.modelChange.emit(this.model)
    }, 1)
  }

  @HostListener('keyup', ['$event'])
  switchLanguage(event) {
    if (event.shiftKey) {
      if (event.keyCode === 37) {
        // left
        const i = this.availableLocales.indexOf(this.selectedLocale) - 1
        if (i >= 0) {
          this.selectedLocale = this.availableLocales[i]
        } else {
          this.selectedLocale = this.availableLocales[
            this.availableLocales.length - 1
          ]
        }
        // @ts-ignore
        const element = this.refs._results.find((e) => {
          return e.nativeElement.dataset.locale === this.selectedLocale
        })
        if (element.nativeElement) {
          nextTick(() => element.nativeElement.focus())
        }
      } else if (event.keyCode === 39) {
        // right
        const i = this.availableLocales.indexOf(this.selectedLocale) + 1
        if (i <= this.availableLocales.length - 1) {
          this.selectedLocale = this.availableLocales[i]
        } else {
          this.selectedLocale = this.availableLocales[0]
        }
        // @ts-ignore
        const element = this.refs._results.find((e) => {
          return e.nativeElement.dataset.locale === this.selectedLocale
        })
        if (element.nativeElement) {
          nextTick(() => element.nativeElement.focus())
        }
      }
    }
  }

  setLocale(locale: String) {
    this.selectedLocale = locale
  }

  buttonColor(locale: String) {
    return this.selectedLocale === locale ? 'primary' : ''
  }
}
