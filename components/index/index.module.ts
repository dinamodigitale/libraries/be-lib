import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { IndexComponent } from './index.component'
import { IndexListDirective } from './index-list.directive'
import { IndexCardDirective } from './index-card.directive'
import { MatListModule } from '@angular/material/list'
import { MatDividerModule } from '@angular/material/divider'
import { MatIconModule } from '@angular/material/icon'
import { FlexLayoutModule } from '@angular/flex-layout'
import { MatButtonModule } from '@angular/material/button'
import { MatCardModule } from '@angular/material/card'
import { MatPaginatorModule } from '@angular/material/paginator'

@NgModule({
  declarations: [IndexComponent, IndexListDirective, IndexCardDirective],
  imports: [
    CommonModule,
    MatListModule,
    MatDividerModule,
    MatIconModule,
    FlexLayoutModule,
    MatButtonModule,
    MatCardModule,
    MatPaginatorModule,
  ],
  exports: [IndexComponent, IndexListDirective, IndexCardDirective],
})
export class IndexModule {}
