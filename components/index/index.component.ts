import {
  Component,
  OnInit,
  ContentChild,
  TemplateRef,
  Input,
  Output,
  EventEmitter,
} from '@angular/core'
import { IndexListDirective } from './index-list.directive'
import { IndexCardDirective } from './index-card.directive'
import { IndexPaginationInterface } from '@be-lib/services/api-service/rest-component.class'
import { PageEvent } from '@angular/material/paginator'

@Component({
  selector: 'dbe-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent implements OnInit {
  @Input() items: any[]
  @Input() title: string
  @ContentChild(IndexListDirective, { read: TemplateRef })
  listTemplate: TemplateRef<any>
  @ContentChild(IndexCardDirective, { read: TemplateRef })
  cardTemplate: TemplateRef<any>

  @Input() view: 'list' | 'card' = 'list'
  @Input() disableLayoutSwitch: boolean
  @Input() enablePagination: boolean
  @Input() pagination: IndexPaginationInterface
  @Input() pageSizeOptions: Array<number> = [25, 50, 100]
  @Output() pageChange: EventEmitter<PageEvent> = new EventEmitter()

  constructor() {}

  ngOnInit(): void {}
}
