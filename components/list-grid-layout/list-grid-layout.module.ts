import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ListGridLayoutComponent } from './list-grid-layout.component'
import { UIRouterModule } from '@uirouter/angular'
import { MatIconModule } from '@angular/material/icon'
import { MatButtonModule } from '@angular/material/button'
import { MatCardModule } from '@angular/material/card'
import { MatListModule } from '@angular/material/list'
import { MatPaginatorModule } from '@angular/material/paginator'
import { FlexLayoutModule } from '@angular/flex-layout'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input'
import { FormsModule } from '@angular/forms'
import { ConfirmModule } from '@be-lib/directives/confirm/confirm.module'
import { IndexFiltersModule } from '../index-filters/index-filters.module'
import { MatSelectModule } from '@angular/material/select'

@NgModule({
  declarations: [ListGridLayoutComponent],
  imports: [
    CommonModule,
    UIRouterModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatListModule,
    MatPaginatorModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ConfirmModule,
    MatSelectModule,
    IndexFiltersModule,
  ],
  exports: [ListGridLayoutComponent],
})
export class ListGridLayoutModule {}
