import {
  Directive,
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ContentChild,
  TemplateRef,
} from '@angular/core'
import { StateService } from '@uirouter/core'
import { config } from '@app/environments/environment'

const DEBOUNCE_TIME = 1200

@Directive({
  selector: '[content]',
})
export class Content {}

@Directive({
  selector: '[preview]',
})
export class Preview {}

@Component({
  selector: 'dbe-list-grid-layout',
  templateUrl: './list-grid-layout.component.html',
  styleUrls: ['./list-grid-layout.component.scss'],
})
export class ListGridLayoutComponent implements OnInit {
  @Input() cardTitle: string
  @Input() cardSubTitle: string
  @Input() items: any[]
  @Input() hasMaterials: boolean = false
  @Input() hasPreview: boolean = false
  @Input() renderer: Function | undefined
  @Input() filters

  @Input() uiSrefEditState = '^.edit'

  @Input() disableDelete: boolean = false
  @Input() disableNew: boolean = false
  @Input() disableSearch: boolean = false

  @ContentChild(Content, { read: TemplateRef })
  contentTemplate: TemplateRef<any>

  @ContentChild(Preview, { read: TemplateRef })
  previewTemplate: TemplateRef<any>

  @Output() searchText: EventEmitter<any> = new EventEmitter()
  @Output() delete: EventEmitter<any> = new EventEmitter()
  @Output() sendFilter: EventEmitter<any> = new EventEmitter()

  gridView: false
  debounce: any = 0
  keyword: string = ''

  basePath = `${config.api.path}/`

  constructor(private stateService: StateService) {
    this.keyword = this.stateService.params.keyword
  }

  search(event): void {
    clearTimeout(this.debounce)
    this.debounce = setTimeout(() => {
      this.searchText.emit(event)
    }, DEBOUNCE_TIME)
  }

  resetSearch() {
    this.stateService.go('.', { keyword: '', page: 0 }, { reload: true })
  }

  deleteItem(item) {
    if (item) {
      this.delete.emit(item)
    }
  }

  filterResults($event) {
    this.sendFilter.emit($event)
  }

  ngOnInit(): void {}
}
