import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'

@Component({
  selector: 'dbe-view-frame',
  templateUrl: './view-frame.component.html',
  styleUrls: ['./view-frame.component.scss'],
})
export class ViewFrameComponent implements OnInit {
  @Input() heading: string
  @Input() subTitle: string
  @Input() disableBack: boolean = true
  @Input() back: string = '^.index'
  @Input() backParams: any = {}
  @Input() loading: boolean = true
  @Input() disableSidebar: boolean
  @Input() enableNew: boolean
  @Input() newButtonText: string = 'Nuovo'
  @Input() newButtonPath: string = '^.new'
  @Input() newButtonParams: any = {}

  constructor() {}

  ngOnInit(): void {}
}
