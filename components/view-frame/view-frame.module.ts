import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ViewFrameComponent } from './view-frame.component'
import { MatButtonModule } from '@angular/material/button'
import { MatIconModule } from '@angular/material/icon'
import { MatCardModule } from '@angular/material/card'
import { UIRouterModule } from '@uirouter/angular'
import { FlexLayoutModule } from '@angular/flex-layout'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'

@NgModule({
  declarations: [ViewFrameComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    UIRouterModule,
    FlexLayoutModule,
    MatProgressSpinnerModule,
  ],
  exports: [ViewFrameComponent],
})
export class ViewFrameModule {}
