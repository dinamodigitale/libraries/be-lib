import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { ApiServiceInterface } from '@be-lib/services/api-service/rest-component.class'

@Component({
  selector: 'dbe-entity-select[apiService]',
  templateUrl: './dinamo-entity-select.component.html',
  styleUrls: ['./dinamo-entity-select.component.scss'],
})
export class DinamoEntitySelectComponent implements OnInit {
  @Input() apiService: ApiServiceInterface<any>
  @Input() enableReset = false
  @Input() label: string
  @Input() multiple = false
  @Input() value
  @Output() selectionChange = new EventEmitter()
  items: any[] = []
  constructor() {}

  ngOnInit(): void {
    this.apiService
      .resource(null, { params: { limit: 1000 } })
      .subscribe((res) => {
        // @ts-ignore
        this.items = this.formatValues(res.pagination ? res.data : res)
      })
  }

  private formatValues(items): any[] {
    return items.map((item) => {
      return {
        value: item._id,
        text: item.name,
      }
    })
  }

  selection($event) {
    this.selectionChange.emit($event.value)
  }
}
