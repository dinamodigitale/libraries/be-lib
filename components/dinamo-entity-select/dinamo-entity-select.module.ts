import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { DinamoEntitySelectComponent } from './dinamo-entity-select.component'
import { MatSelectModule } from '@angular/material/select'

@NgModule({
  declarations: [DinamoEntitySelectComponent],
  imports: [CommonModule, MatSelectModule],
  exports: [DinamoEntitySelectComponent],
})
export class DinamoEntitySelectModule {}
