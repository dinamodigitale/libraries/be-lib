import {
  Component,
  OnInit,
  Input,
  TemplateRef,
  ContentChild,
  Output,
  EventEmitter,
} from '@angular/core'
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop'

@Component({
  selector: 'dbe-dragger',
  templateUrl: './dinamo-dragger.component.html',
  styleUrls: ['./dinamo-dragger.component.scss'],
})
export class DinamoDraggerComponent implements OnInit {
  @Input() items
  @Input() enableEdit = false
  @Input() enableRemove = false
  @Input() enableDrag = true
  @Input() enableDeleteConfirmMessage = false
  @Input() isIndex = false
  @ContentChild(TemplateRef) itemTemplate: TemplateRef<any>
  @Output() edit = new EventEmitter()
  @Output() remove = new EventEmitter()
  @Output() dragged = new EventEmitter()
  @Output() create = new EventEmitter()
  constructor() { }

  ngOnInit(): void { }

  public drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);

    }

    // moveItemInArray(
    //   event.container.data,
    //   event.previousIndex,
    //   event.currentIndex
    // )
    this.dragged.emit(this.items)
  }

  editClick(index) {
    this.edit.emit(index)
  }

  removeClick(index) {
    this.remove.emit(index)
  }

  createNewItem($event) {
    this.create.emit()
  }
}
