import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { DinamoDraggerComponent } from './dinamo-dragger.component'
import { DragDropModule } from '@angular/cdk/drag-drop'
import { FlexLayoutModule } from '@angular/flex-layout'
import { MatIconModule } from '@angular/material/icon'
import { ConfirmModule } from '@be-lib/directives/confirm/confirm.module'
import { MatButtonModule } from '@angular/material/button'

@NgModule({
  declarations: [DinamoDraggerComponent],
  imports: [
    CommonModule,
    DragDropModule,
    FlexLayoutModule,
    MatIconModule,
    ConfirmModule,
    MatButtonModule,
  ],
  exports: [DinamoDraggerComponent],
})
export class DinamoDraggerModule {}
