import { Injectable } from '@angular/core'
import { Restify } from '@be-lib/services/api-service/restify.class'
import { ApiService } from '@be-lib/services/api-service/api.service'

@Injectable({
  providedIn: 'root',
})
export class DinamoResourceService extends Restify<any> {
  basePath = '/backend/resources'

  constructor(protected apiService: ApiService) {
    super(apiService)
  }
}
