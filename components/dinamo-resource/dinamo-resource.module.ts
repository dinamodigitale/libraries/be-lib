import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { BrowserModule } from '@angular/platform-browser'
import { DinamoUploadsComponent } from './dinamo-uploads/dinamo-uploads.component'
import { MatButtonModule } from '@angular/material/button'
import { FlexLayoutModule } from '@angular/flex-layout'
import { MatSelectModule } from '@angular/material/select'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatCheckboxModule } from '@angular/material/checkbox'
import { MatInputModule } from '@angular/material/input'
import { I18nInputModule } from '../i18n-input/i18n-input.module'
import { MatIconModule } from '@angular/material/icon'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { DinamoResourceComponent } from './dinamo-resource/dinamo-resource.component'
import { DinamoResourcesComponent } from './dinamo-resources/dinamo-resources.component'
import { MatTooltipModule } from '@angular/material/tooltip'
import { MatSlideToggleModule } from '@angular/material/slide-toggle'

@NgModule({
  declarations: [
    DinamoResourceComponent,
    DinamoUploadsComponent,
    DinamoResourcesComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BrowserModule,
    MatButtonModule,
    FlexLayoutModule,
    MatSelectModule,
    FormsModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatInputModule,
    I18nInputModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatSlideToggleModule
  ],
  exports: [
    DinamoResourceComponent,
    DinamoUploadsComponent,
    DinamoResourcesComponent,
  ],
})
export class DinamoResourceModule {}
