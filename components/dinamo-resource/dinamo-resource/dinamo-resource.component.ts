import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  AfterViewInit,
} from '@angular/core'
import { DinamoResourceService } from '../dinamo-resource.service'
import { SnackService } from '@be-lib/services/snack.service'
import { config } from '@app/environments/environment'
import { DinamoUploadsComponent } from '../dinamo-uploads/dinamo-uploads.component'
import { TransitionHook } from '@uirouter/core'

export type ResourceType = 'images' | 'videos' | 'attachments'

@Component({
  selector: 'dbe-resource',
  templateUrl: './dinamo-resource.component.html',
  styleUrls: ['./dinamo-resource.component.scss'],
})
export class DinamoResourceComponent implements OnInit, AfterViewInit {
  @Input() resource: any
  @Input() resources: string[]
  @Input() types: ResourceType[] = ['images']
  @Input() title = 'Risorsa'
  @Output() resourceChange = new EventEmitter()
  @Output() resourcesChange = new EventEmitter()
  @Output() save = new EventEmitter()
  @Input() i18n: boolean = true
  @Input() cols = 2
  @Input() single: boolean
  @Input() disabled: boolean
  @ViewChild(DinamoUploadsComponent) uploadsComponent: DinamoUploadsComponent
  @ViewChild('resourceBox') resourceBox: ElementRef<HTMLElement>
  mode: 'edit' | 'preview' = 'preview'
  type: ResourceType
  data: any = {}
  resKey = 'images'
  newVideo = {
    autoplay: false,
    fullPath: '',
    width: 1920,
    height: 1080,
  }
  basePath = `${config.api.path}/`
  labels = {
    images: 'Media',
    attachments: 'Allegati',
    videos: 'Video',
  }

  uploadVideos: boolean = false

  @Input() newResource: boolean

  dragOver = false

  label: string = ''

  constructor(
    private resourceService: DinamoResourceService,
    private snackService: SnackService
  ) {}

  ngOnInit(): void {
    if (this.resource) {
      this.resourceService
        .get(this.resource._id || this.resource)
        .subscribe((res) => {
          this.data = res
          this.type = this.checkType()
        })
    } else {
      // this.mode = 'edit'
      if (this.i18n) {
        this.data = {
          name: {
            it: this.title,
          },
          images: [],
          attachments: [],
          videos: [],
        }
      } else {
        this.data = {
          name: this.title,
          images: [],
          attachments: [],
          videos: [],
        }
      }
      this.type = this.checkType()
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      if (this.resourceBox && this.resourceBox.nativeElement) {
        ;['dragenter', 'dragover', 'dragleave', 'drop'].forEach((eventName) => {
          this.resourceBox.nativeElement.addEventListener(
            eventName,
            this.dragDropPrevent,
            false
          )
        })
        ;['dragenter', 'dragover'].forEach((eventName) => {
          this.resourceBox.nativeElement.addEventListener(
            eventName,
            ($event) => this.dragDropOver($event),
            false
          )
        })
        this.resourceBox.nativeElement.addEventListener(
          'dragleave',
          ($event) => this.dragDropLeave($event),
          false
        )
        this.resourceBox.nativeElement.addEventListener(
          'drop',
          ($event) => this.dragDropUpload($event),
          false
        )
      }
    })
  }

  private dragDropOver(e) {
    if (!this.disabled) {
      this.dragOver = true
    }
  }

  private dragDropLeave(e) {
    if (!this.disabled) {
      this.dragOver = false
    }
  }

  private dragDropUpload(e) {
    if (!this.disabled) {
      this.dragOver = false
      this.uploadsComponent
      this.uploadsComponent.onSelectedFile({
        target: {
          files: e.dataTransfer.files,
        },
      })
    }
  }

  private dragDropPrevent(e: any) {
    if (!this.disabled) {
      e.preventDefault()
      e.stopPropagation()
    }
  }

  checkType() {
    let resourcesType = this.types[0]
    const types: ResourceType[] = ['videos', 'images', 'attachments']
    types.map((type) => {
      if (this.data && this.data[type] && this.data[type].length) {
        resourcesType = type
      }
    })
    return resourcesType
  }

  async update(close = false) {
    this.data.name = this.data.name || undefined
    this.data.description = this.data.description || undefined

    return this.resourceService.update(this.data._id, this.data).subscribe(
      (res) => {
        this.data = res
        this.resource = this.data._id
        this.resourceChange.emit(this.resource)
        if (this.resources) {
          this.resourcesChange.emit(this.resources)
        }
        this.save.emit()
        if (close) {
          this.mode = 'preview'
        }
        this.snackService.info('Risorsa aggiornata con successo')
      },
      (e) => {
        this.snackService.error(e)
      }
    )
  }

  delete(index) {
    this.data[this.type].splice(index, 1)
    this.update()
  }

  uploaded(file?: any) {
    if (this.type === 'videos' && !this.uploadVideos) {
      this.data.videos.push(this.newVideo)
      this.newVideo = {
        autoplay: false,
        fullPath: '',
        width: 1920,
        height: 1080,
      }
    } else {
      this.data[this.type].push(file)
    }
    const types = ['videos', 'images', 'attachments']
    types.map((type) => {
      if (type !== this.type) {
        this.data[type] = []
      }
    })

    if (this.data._id) {
      this.update()
    } else {
      this.resourceService.create(this.data).subscribe((res) => {
        this.data = res
        this.resource = this.data._id
        this.resourceChange.emit(this.resource)
        if (this.resources) {
          this.resources.push(this.resource)
          this.resourcesChange.emit(this.resources)
        }
        this.save.emit()
        this.uploadVideos = false
      })
    }
  }

  get resShortDescription() {
    if (!this.data || !this.data.description) {
      return 'Nessuna descrizione'
    }
    let string = ''
    if (this.i18n) {
      string = this.data.description.it || this.data.description.en
    } else {
      string = this.data.description
    }
    if (string) {
      return string.length > 250 ? string.substring(0, 250) + '...' : string
    }
    return ''
  }

  get resTitle() {
    if (!this.data) {
      return 'Nessun titolo'
    }
    if (this.i18n) {
      return this.data.name.it || this.data.name.en
    } else {
      return this.data.name
    }
  }

  get canSave() {
    return (
      this.resource !== null &&
      this.data &&
      this.data[this.type] &&
      this.data[this.type].length
    )
  }
}
