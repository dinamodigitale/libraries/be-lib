import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Input,
  Output,
} from '@angular/core'
import { FormBuilder, FormGroup } from '@angular/forms'
import { FileUploadService } from './file-upload.service'
import { EventEmitter } from '@angular/core'

@Component({
  selector: 'dbe-uploads',
  templateUrl: './dinamo-uploads.component.html',
  styleUrls: ['./dinamo-uploads.component.scss'],
})
export class DinamoUploadsComponent implements OnInit {
  @ViewChild('fileInput') fileInput: ElementRef

  @Input() responseFilePath: String = 'file.path'
  @Input() responseFileName: String = 'file.fileName'
  @Input() responseFileSize: String = 'file.size'
  @Input() responseFileMime: String = 'file.mimeType'

  @Input() showTooltip: Boolean = false
  @Input() tooltipText: String =
    "Devi salvare per associare l'immagine alla pagina"

  @Input() titleText: String = 'File Upload'

  @Input() uploadedFile: any = null
  @Output() uploaded: EventEmitter<Object> = new EventEmitter()

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onUploadChange: EventEmitter<Object> = new EventEmitter()

  selectedFile: File = null

  form: FormGroup
  error: string

  fileUploaderMessage = { status: '', message: '' }

  constructor(
    private fb: FormBuilder,
    private fileUploadService: FileUploadService
  ) {}

  ngOnInit() {
    this.form = this.fb.group({
      attachment: [''],
    })
  }

  get uploadedFilePath(): String {
    const result = this.getResponseFileProperty(this.responseFilePath)
    return `${this.fileUploadService.attachmentsUrl}/${result}`
  }
  get uploadedFileName(): String {
    return this.getResponseFileProperty(this.responseFileName)
  }
  get uploadedFileSize(): Number {
    return this.getResponseFileProperty(this.responseFileSize)
  }
  get uploadedFileType(): String {
    return this.getResponseFileProperty(this.responseFileMime)
  }
  get hasUploadableFile() {
    if (!this.selectedFile) {
      return false
    } else if (this.selectedFile && !this.uploadedFile) {
      return true
    } else if (
      this.selectedFile &&
      this.uploadedFile &&
      this.selectedFileIsSameAsUploadedFile
    ) {
      return false
    } else {
      return true
    }
  }
  get hasUploadedFile() {
    return this.uploadedFile && Object.keys(this.uploadedFile).length
  }

  fileIsImage() {
    return this.uploadedFileType.indexOf('image') >= 0
  }
  removeUploadedFile() {
    this.selectedFile = null
    this.uploadedFile = null
    this.uploaded.emit({})
    this.onUploadChange.emit({})
  }
  toMB(size: any) {
    return (size / 1024 / 1024).toFixed(2)
  }

  openChooseFile() {
    this.simulateClick(this.fileInput.nativeElement)
  }
  onSelectedFile(event: any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0]
      this.form.get('attachment').setValue(file)
      this.selectedFile = file
    }
  }
  onSubmit() {
    const formData = new FormData()
    formData.append('attachment', this.form.get('attachment').value)
    this.fileUploadService.upload(formData).subscribe(
      (res) => {
        this.fileUploaderMessage = res
        if (res.status === 'done') {
          this.uploadedFile = res.message
          this.uploaded.emit(this.uploadedFile)
          this.onUploadChange.emit({})
          this.selectedFile = null
          this.uploadedFile = null
        }
      },
      (err) => {
        this.error = err
      }
    )
  }

  private simulateClick(elem: any) {
    return !elem.dispatchEvent(
      new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
        view: window,
      })
    )
  }
  private getResponseFileProperty(property: String) {
    if (!this.uploadedFile || !Object.keys(this.uploadedFile).length) {
      return ''
    }
    let result = this.uploadedFile
    property.split('.').forEach((e) => {
      result = this.uploadedFile[e]
    })
    return result
  }
  private get selectedFileIsSameAsUploadedFile() {
    if (this.selectedFile && this.uploadedFile) {
      const uploadedIdentifier = `${this.uploadedFileName}${this.uploadedFileSize}${this.uploadedFileType}`
      const selectedIdentifier = `${this.selectedFile.name}${this.selectedFile.size}${this.selectedFile.type}`
      return uploadedIdentifier === selectedIdentifier
    } else {
      return null
    }
  }
}
