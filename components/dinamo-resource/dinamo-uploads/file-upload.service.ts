import { Injectable } from '@angular/core'
import {
  HttpClient,
  HttpEvent,
  HttpErrorResponse,
  HttpEventType,
} from '@angular/common/http'
import { throwError } from 'rxjs'
import { catchError, map } from 'rxjs/operators'
import { config } from '@app/environments/environment'

@Injectable({
  providedIn: 'root',
})
export class FileUploadService {
  config = {
    basePath: config.api.fullPath,
    path: '/backend/uploads',
    attachmentsPath: config.api.path,
  }

  constructor(private http: HttpClient) {}

  setApiPath(path) {
    this.config.path = path
    return this
  }
  setAttachmentsPath(path) {
    this.config.attachmentsPath = path
    return this
  }

  private get apiUrl() {
    return `${this.config.basePath}${this.config.path}`
  }
  public get attachmentsUrl() {
    return this.config.attachmentsPath
  }

  upload(formData) {
    return this.http
      .post<any>(`${this.apiUrl}`, formData, {
        reportProgress: true,
        observe: 'events',
      })
      .pipe(
        map((event) => this.getEventMessage(event, formData)),
        catchError(this.handleError)
      )
  }

  private getEventMessage(event: HttpEvent<any>, formData) {
    switch (event.type) {
      case HttpEventType.UploadProgress:
        return this.fileUploadProgress(event)
      case HttpEventType.Response:
        return this.apiResponse(event)
      default:
        return {
          status: 'requesting',
          message: '',
        }
    }
  }

  private fileUploadProgress(event) {
    return {
      status: 'progress',
      message: Math.round((100 * event.loaded) / event.total),
    }
  }

  private apiResponse(event) {
    return {
      status: 'done',
      message: event.body,
    }
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message)
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      )
    }
    return throwError('Something bad happened. Please try again later.')
  }
}
