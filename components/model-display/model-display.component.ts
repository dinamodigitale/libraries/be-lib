import { Component, OnInit, Input } from '@angular/core'

@Component({
  selector: 'dbe-model-display',
  templateUrl: './model-display.component.html',
  styleUrls: ['./model-display.component.scss'],
})
export class ModelDisplayComponent implements OnInit {
  @Input() model: any
  @Input() label: string
  @Input() showText: boolean

  constructor() {}

  ngOnInit(): void {}
}
