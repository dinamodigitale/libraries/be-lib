import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ModelDisplayComponent } from './model-display.component'

@NgModule({
  declarations: [ModelDisplayComponent],
  imports: [CommonModule],
  exports: [ModelDisplayComponent],
})
export class ModelDisplayModule {}
