import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { ApiServiceInterface } from '@be-lib/services/api-service/rest-component.class'
import { Observable } from 'rxjs'
import { FormControl } from '@angular/forms'
import { startWith, map } from 'rxjs/operators'

@Component({
  selector: 'dbe-autocomplete[apiService]',
  templateUrl: './dinamo-autocomplete.component.html',
  styleUrls: ['./dinamo-autocomplete.component.scss'],
})
export class DinamoAutocompleteComponent implements OnInit {
  @Input() apiService: ApiServiceInterface<any>
  @Input() label: string
  @Output() selectionChange = new EventEmitter()
  myControl = new FormControl('')
  items: any[] = []
  filteredItems: Observable<any[]>
  constructor() {
    this.myControl.valueChanges.subscribe((value) => {
      console.log(value)
    })
  }

  ngOnInit(): void {
    console.log(this.filteredItems)
    this.apiService.index().subscribe((res) => {
      // @ts-ignore
      this.items = this.formatValues(res.pagination ? res.data : res)
      setTimeout(() => {})
      //@ts-ignore
      this.filteredItems = this.myControl.valueChanges.pipe(
        startWith(''),
        map((value) => (typeof value === 'string' ? value : value.text)),
        map((id) => (id ? this._filter(id) : this.items.slice()))
      )
    })
  }

  private formatValues(items): any[] {
    return items.map((item) => {
      return {
        id: item._id,
        text: item.name.it || item.name.en,
      }
    })
  }

  private _filter(id): any[] {
    return this.items.filter((item) => item.id.indexOf(id) === 0)
  }

  displayFn(item): string {
    return item && item.text ? item.text : ''
  }

  selection($event) {
    this.selectionChange.emit($event)
  }
}
