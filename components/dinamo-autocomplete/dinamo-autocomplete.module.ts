import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { DinamoAutocompleteComponent } from './dinamo-autocomplete.component'
import { MatAutocompleteModule } from '@angular/material/autocomplete'
import { MatInputModule } from '@angular/material/input'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

@NgModule({
  declarations: [DinamoAutocompleteComponent],
  imports: [
    CommonModule,
    MatAutocompleteModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [DinamoAutocompleteComponent],
})
export class DinamoAutocompleteModule {}
