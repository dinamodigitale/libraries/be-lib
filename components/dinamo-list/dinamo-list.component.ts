import { Component, OnInit, Input } from '@angular/core'

@Component({
  selector: 'dbe-list[items]',
  templateUrl: './dinamo-list.component.html',
  styleUrls: ['./dinamo-list.component.scss'],
})
export class DinamoListComponent implements OnInit {
  @Input() items: any[]
  @Input() title: string
  @Input() subTitle: string

  constructor() {}

  ngOnInit() {}
}
