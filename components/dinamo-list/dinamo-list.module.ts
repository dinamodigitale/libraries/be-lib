import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { DinamoListComponent } from './dinamo-list.component'
import { MatListModule } from '@angular/material/list'
import { MatIconModule } from '@angular/material/icon'
import { MatButtonModule } from '@angular/material/button'
import { MatCardModule } from '@angular/material/card'
import { MatPaginatorModule } from '@angular/material/paginator'

@NgModule({
  declarations: [DinamoListComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatListModule,
    MatPaginatorModule,
  ],
  exports: [DinamoListComponent],
})
export class DinamoListModule {}
