import { Injectable } from '@angular/core'

export interface I18nChipInterface {
  [x: string]: string | boolean
  _i18n: boolean
}

export interface I18nFoundChipInterface {
  [x: string]: any
}

@Injectable()
export class DevilChipToolsService {
  private method = 'find'
  private searching: I18nChipInterface
  private arrayOfResearch: Array<any>

  constructor() {}

  find(selectedChip: I18nChipInterface, method?: string) {
    if (!method) {
      this.method = 'find'
    } else {
      this.method = method
    }
    this.searching = selectedChip
    return this
  }
  in(chips: Array<any>) {
    this.arrayOfResearch = chips
    return this
  }
  withKey(key: string) {
    return this.arrayOfResearch[this.method](
      (t: any) => t[key] === this.searching.devilChipToken
    )
  }

  setObjectToken(chip: I18nChipInterface, token: string | number) {
    Object.defineProperty(chip, 'devilChipToken', {
      value: token,
      enumerable: false,
    })
  }

  setObjectsToken(chips: Array<I18nChipInterface>, token: string | number) {
    chips.forEach((_, i: number) => {
      this.setObjectToken(chips[i], token)
    })
  }
}
