import { COMMA, ENTER } from '@angular/cdk/keycodes'
import {
  Component,
  ElementRef,
  ViewChild,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChildren,
} from '@angular/core'
import { FormControl } from '@angular/forms'
import { Observable } from 'rxjs'
import { map, startWith } from 'rxjs/operators'
import { config } from '@app/environments/environment'
import { I18nDevilChipDialogComponent } from './i18n-devil-chip-dialog/i18n-devil-chip-dialog.component'
import { MatDialog } from '@angular/material/dialog'
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete'
import { MatChipInputEvent } from '@angular/material/chips'
import { ApiServiceInterface } from '@be-lib/services/api-service/rest-component.class'

@Component({
  selector: 'dbe-devil-chip',
  templateUrl: './devil-chip.component.html',
  styleUrls: ['./devil-chip.component.scss'],
})
export class DevilChipComponent implements OnInit {
  visible = true
  selectable = true
  removable = true
  addOnBlur = false

  availableLocales: Array<string>

  separatorKeysCodes: number[] = [ENTER, COMMA]
  elementCtrl = new FormControl()

  filteredElements: Observable<string[]>
  @Input() newElementsAvailable = true
  @Input() selectedElements: Array<any> = []
  @Input() availableElements: Array<any> = []

  // tslint:disable-next-line:no-inferrable-types
  @Input() placeholder: string = 'Element'
  // tslint:disable-next-line:no-inferrable-types
  @Input() isI18n: boolean | Array<string> = false

  @Input() apiService: ApiServiceInterface<any>

  @Input() enableEdit = true

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onNewElement: EventEmitter<any> = new EventEmitter<any>()
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onSelectElement: EventEmitter<any> = new EventEmitter<any>()
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onUpdateElement: EventEmitter<any> = new EventEmitter<any>()
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onDeleteElement: EventEmitter<any> = new EventEmitter<any>()

  get allElements() {
    console.log(this.availableElements)
    if (!this.isI18n) {
      return this.availableElements.filter((x) => {
        return !this.selectedElements.includes(x)
      })
    } else {
      return this.availableElements.filter((x) => {
        return !this.selectedElements
          .map((se) => JSON.stringify(se))
          .includes(JSON.stringify(x))
      })
    }
  }

  @ViewChild('elementInput') elementInput: ElementRef
  @ViewChildren('chip') chip: ElementRef

  constructor(public dialog: MatDialog) {}

  ngOnInit() {
    this.assignFilteredElements()

    if (Array.isArray(this.isI18n)) {
      this.availableLocales = this.isI18n
    } else if (this.isI18n === true) {
      this.availableLocales = config.i18n.availableLocales
    }
  }

  labelize(element) {
    if (typeof element === typeof 'string') {
      return element
    } else if (typeof element.name === typeof 'string') {
      return element.name
    } else {
      return element.name[config.i18n.defaultLocale]
    }
  }

  private assignFilteredElements() {
    this.filteredElements = this.elementCtrl.valueChanges.pipe(
      startWith(null),
      map((element: string | null) => {
        return element ? this._filter(element) : this.allElements.slice()
      })
    )
  }

  openModal(tag: Object, emitter: string) {
    if (!this.enableEdit) {
      return
    }
    const self = this

    if (this.isI18n) {
      // questo caso avviene quando si crea un nuovo tag
      if (typeof tag === typeof 'string') {
        tag = {
          name: {
            [config.i18n.defaultLocale]: tag,
            _i18n: true,
          },
        }
      }

      const dialogRef = this.dialog.open(I18nDevilChipDialogComponent, {
        width: '80vw',
        data: tag,
      })

      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          if (this.apiService) {
            let subscription
            if (result._id) {
              subscription = this.apiService.update(result._id, result)
            } else {
              subscription = this.apiService.create(result)
            }
            subscription.subscribe((res) => {
              if (self[emitter]) {
                console.log('emitter', self[emitter])
                self[emitter].emit(res)
              }

              if (emitter === 'onNewElement') {
                this.availableElements.push(res)
                this.selectedElements.push(res)
              }
            })
          } else {
            if (self[emitter]) {
              self[emitter].emit(result)
            }

            if (emitter === 'onNewElement') {
              this.availableElements.push(result)
              this.selectedElements.push(result)
            }
          }
        }
      })
    }
  }

  existsElement(element) {
    return !!this.availableElements.find((u) => u === element)
  }

  remove(element: string): void {
    const index = this.selectedElements.indexOf(element)

    if (index >= 0) {
      this.selectedElements.splice(index, 1)
      this.assignFilteredElements()
      this.onDeleteElement.emit(element)
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    let selected = event.option.viewValue

    if (this.isI18n) {
      selected = this.allElements.find((el) => {
        return el.name[config.i18n.defaultLocale] === event.option.viewValue
      })
    }

    this.onSelectElement.emit(selected)

    this.selectedElements.push(selected)
    this.elementInput.nativeElement.value = ''
    this.elementCtrl.setValue(null)
  }

  entered(event: MatChipInputEvent): void {
    if (this.newElementsAvailable) {
      const tag = event.value.trim()
      if (tag) {
        if (this._filter(tag).length) {
          this.onNewElement.emit(tag)
        } else {
          this.openModal(tag, 'onNewElement')
          if (!this.isI18n) {
            this.availableElements.push(tag)
          }
        }

        this.assignFilteredElements()

        this.elementInput.nativeElement.value = ''
        this.elementCtrl.setValue(null)

        if (!this.isI18n) {
          this.selectedElements.push(tag)
        }
      }
    }
  }

  private _filter(value: any): any[] {
    let filterValue
    try {
      filterValue = value.name[config.i18n.defaultLocale].toLowerCase().trim()
    } catch (e) {
      filterValue = value.toLowerCase().trim()
    }

    return this.allElements.filter((element) => {
      try {
        return (
          element.name[config.i18n.defaultLocale]
            .toLowerCase()
            .indexOf(filterValue) === 0
        )
      } catch (e) {
        return element.name.toLowerCase().indexOf(filterValue) === 0
      }
    })
  }

  getColor(element) {
    if (typeof element === typeof {} && !Array.isArray(element)) {
      if (!element.name[config.i18n.defaultLocale]) {
        return 'languages-in-error'
      } else {
        return !this.availableLocales.find((l) => {
          return !element.name[l]
        })
          ? 'languages-complete'
          : 'languages-non-complete'
      }
    } else {
      return null
    }
  }
}
