import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { DevilChipComponent } from './devil-chip.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { I18nDevilChipDialogComponent } from './i18n-devil-chip-dialog/i18n-devil-chip-dialog.component'
import { DevilChipToolsService } from './devil-chip-tools.service'
import { MatChipsModule } from '@angular/material/chips'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatAutocompleteModule } from '@angular/material/autocomplete'
import { MatIconModule } from '@angular/material/icon'
import { FlexLayoutModule } from '@angular/flex-layout'
import { MatInputModule } from '@angular/material/input'
import { MatButtonModule } from '@angular/material/button'

@NgModule({
  declarations: [DevilChipComponent, I18nDevilChipDialogComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatChipsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatIconModule,
    FlexLayoutModule,
    MatInputModule,
    MatButtonModule,
  ],
  exports: [DevilChipComponent],
  providers: [DevilChipToolsService],
  entryComponents: [I18nDevilChipDialogComponent],
})
export class DevilChipModule {}
