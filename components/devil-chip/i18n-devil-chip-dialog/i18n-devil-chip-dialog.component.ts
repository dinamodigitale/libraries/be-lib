import { Component, Inject } from '@angular/core'
import { config } from '@app/environments/environment'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'

@Component({
  selector: 'dbe-i18n-devil-chip-dialog',
  templateUrl: './i18n-devil-chip-dialog.component.html',
  styleUrls: ['./i18n-devil-chip-dialog.component.scss'],
})
export class I18nDevilChipDialogComponent {
  availableLocales = config.i18n.availableLocales

  constructor(
    public dialogRef: MatDialogRef<I18nDevilChipDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  onNoClick(): void {
    this.dialogRef.close()
  }

  save() {
    this.dialogRef.close(this.data)
  }
}
