import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core'
import { ApiServiceInterface } from '@be-lib/services/api-service/rest-component.class'

@Component({
  selector: 'dbe-index-filters',
  templateUrl: './index-filters.component.html',
  styleUrls: ['./index-filters.component.scss'],
})
export class IndexFiltersComponent implements OnInit {
  apiService: ApiServiceInterface<any>
  @Input() enableReset = false
  @Input() label: string
  @Input() multiple = false
  @Input() value
  @Input() filters: any[]

  @Output() filterActivated: EventEmitter<any> = new EventEmitter()
  items: any[] = []

  activeFilter
  activeItem

  selectedFilter
  constructor() {}

  ngOnInit(): void {}

  private formatValues(items): any[] {
    return items.map((item) => {
      return {
        value: item._id,
        text: item.name,
      }
    })
  }

  selectFilter($event) {
    this.selectedFilter = $event.value
    this.activateFilterOptions($event.value)
  }

  activateFilterOptions(filterName) {
    const item = this.filters.find((res) => res.name === filterName)
    this.apiService = item.service
    this.apiService.index().subscribe((res) => {
      // @ts-ignore
      this.items = this.formatValues(res.pagination ? res.data : res)
    })
  }

  activateFilterCat($event) {
    this.filterActivated.emit($event.value)
  }
}
