import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { IndexFiltersComponent } from './index-filters.component'
import { MatSelectModule } from '@angular/material/select'
import { MatCheckboxModule } from '@angular/material/checkbox'

@NgModule({
  declarations: [IndexFiltersComponent],
  imports: [CommonModule, MatSelectModule, MatCheckboxModule],
  exports: [IndexFiltersComponent],
})
export class IndexFiltersModule {}
