import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { ApiServiceInterface } from '@be-lib/services/api-service/rest-component.class'

@Component({
  selector: 'dbe-suggest[apiService]',
  templateUrl: './dinamo-suggest.component.html',
  styleUrls: ['./dinamo-suggest.component.scss'],
})
export class DinamoSuggestComponent implements OnInit {
  @Input() apiService: ApiServiceInterface<any>
  @Input() placeholder = 'Cerca'
  @Input() selected: any[] = []
  @Input() populate: any[] = []
  @Input() body: any = {}
  @Input() layout: 'inline' | 'columned' = 'columned'
  @Input() isDownload = false
  @Output() select = new EventEmitter()
  timeout
  results = []
  @Input() renderer = (item) => item.name.en || item.name.it || item.name
  @Input() filter = (results, selected) =>
    results.filter((res) => {
      return !selected.map((single) => single).includes(res._id)
    })

  constructor() {}

  ngOnInit() {}

  update(query: string) {
    clearTimeout(this.timeout)
    if (query) {
      this.timeout = setTimeout(() => {
        this.apiService
          .resource({
            query: { $text: { $search: query }, ...this.body },
            populate: this.populate,
          })
          .subscribe((res) => {
            // debugger
            this.results = this.filter(res.data, this.selected)
          })
      }, 500)
    } else {
      this.results = []
    }
  }

  onSelect(item) {
    this.results = this.results.filter((res) => res._id !== item._id)
    this.select.emit(item)
  }
  selectInput($event: Event) {
    $event.stopPropagation()
  }
}
