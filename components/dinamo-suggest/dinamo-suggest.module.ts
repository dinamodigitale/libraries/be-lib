import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { DinamoSuggestComponent } from './dinamo-suggest.component'
import { FormsModule } from '@angular/forms'
import { MatInputModule } from '@angular/material/input'
import { MatChipsModule } from '@angular/material/chips'
import { MatIconModule } from '@angular/material/icon'
import { FlexLayoutModule } from '@angular/flex-layout'

@NgModule({
  declarations: [DinamoSuggestComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatInputModule,
    MatChipsModule,
    MatIconModule,
    FlexLayoutModule,
  ],
  exports: [DinamoSuggestComponent],
})
export class DinamoSuggestModule {}
