import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output,
  ViewEncapsulation,
} from '@angular/core'

@Component({
  selector: 'dbe-html-editor',
  templateUrl: './dinamo-html-editor.component.html',
  styleUrls: ['./dinamo-html-editor.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DinamoHtmlEditorComponent implements OnInit {
  @Input() editorModules: Object = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'], // toggled buttons
      // ['blockquote', 'code-block'],

      // [{ 'header': 1 }, { 'header': 2 }],               // custom button values
      [{ list: 'ordered' }, { list: 'bullet' }],
      // [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
      [{ indent: '-1' }, { indent: '+1' }], // outdent/indent
      // [{ 'direction': 'rtl' }],                         // text direction

      // [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      [{ 'header': [1, 2, 3, 4, false] }],

      // [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
      // [{ 'font': [] }],
      [{ align: [] }],

      ['clean'], // remove formatting button

      ['link' /*, 'image', 'video'*/], // link and image, video
    ],
  }

  @Input() content
  @Input() disabled = false
  @Output() contentChange: EventEmitter<any> = new EventEmitter<any>()

  constructor() {}

  ngOnInit() {}

  onChange() {
    this.contentChange.emit(this.content)
  }
}
