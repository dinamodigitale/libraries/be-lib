import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { DinamoHtmlEditorComponent } from './dinamo-html-editor.component'
import { QuillModule } from 'ngx-quill'
import { FormsModule } from '@angular/forms'

@NgModule({
  declarations: [DinamoHtmlEditorComponent],
  imports: [CommonModule, FormsModule, QuillModule],
  exports: [DinamoHtmlEditorComponent],
})
export class DinamoHtmlEditorModule {}
