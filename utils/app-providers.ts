import { HTTP_INTERCEPTORS } from '@angular/common/http'
import { AuthInterceptor } from '@be-lib/interceptors/auth/auth-interceptor'
import { AUTH_INTERCEPTOR_OPTIONS } from '@be-lib/interceptors/auth/auth-interceptor-options'
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field'

const AppProviders = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true,
  },
  {
    provide: AUTH_INTERCEPTOR_OPTIONS,
    useValue: {
      whitelist: [new RegExp('.+/api/v1/backend/.+')],
      blacklist: [new RegExp('.+/api/v1/auth.+'), new RegExp('.+/public/.+')],
    },
  },
  {
    provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
    useValue: { appearance: 'outline' },
  },
]

export default AppProviders
